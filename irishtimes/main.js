$(function(){

	//header scroll
	var headerHeight = $(".top-bar-header").outerHeight() + $(".middle-bar-header").outerHeight();
	var fullHeaderHeight = $("header").outerHeight();
	var width = $(window).width();

	$(window).scroll(function(){
		width = $(window).width();
		console.log(width);
		if(width>768){
			if($(window).scrollTop() > headerHeight){
				$("body").css("padding-top", fullHeaderHeight+"px");
				$("header").css("top", "-"+headerHeight+"px");
				$("header").addClass("scrolled");
				$(".search-scrolled").addClass("scrolled");
				$(".scroll-logo").addClass("scrolled");
			}
			else{
				$("body").css("padding-top", "0");
				$("header").css("top", "auto");
				$("header").removeClass("scrolled");
				$(".search-scrolled").removeClass("scrolled");
				$(".scroll-logo").removeClass("scrolled");
			}
		}
	});

	var count = 0;
	var numOfStories = $(".main-story-link").length;
	//homepage slider	

	function homeSlider(slideTo){
		$(".main-story-link").removeClass("active");
		$(".main-story-link").eq(slideTo).addClass("active");

		$(".main-story").removeClass("fade-in");
		setTimeout(function(){
			$(".main-story").removeClass("active");
			$(".main-story").eq(slideTo).addClass("active");
			setTimeout(function(){
				$(".main-story").eq(slideTo).addClass("fade-in");
			}, 200);			
		}, 400);
	}

	$(".main-stories").on("click", ".main-story-link", function(){
		count = $(this).index();		
		homeSlider(count);
	});

	$(".left-arrow").click(function(){
		if (count == 0){
			count = numOfStories - 1;
		}
		else{
			count = count - 1;
		}
		homeSlider(count);
	});

	$(".right-arrow").click(function(){
		if (count == numOfStories - 1){
			count = 0;
		}
		else{
			count = count + 1;
		}
		homeSlider(count);
	});

	function linkSizeCheck(width){
		if(width<992){
			$(".main-story-links ul").detach().appendTo(".main-story-links-mobile");
		}
		else{
			$(".main-story-links-mobile ul").detach().appendTo(".main-story-links");
		}
	}

	linkSizeCheck(width);

	$(window).resize(function(){
		width = $(window).width();
		linkSizeCheck(width);
	});

	$("main").click(function(){
		$(".hamburger-btn").removeClass("clicked");
		$(".mobile-menu").removeClass("open");
		setTimeout(function(){
			$(".hamburger-btn").removeClass("active")
		}, 200);
	});

	$(".hamburger-btn").click(function(){
		$(this).toggleClass("clicked");
		$(".mobile-menu").toggleClass("open");
		$that = $(this);
		setTimeout(function(){
			$that.toggleClass("active")
		}, 200);
	});

	// var cat ="";

	// $(".bottom-bar-header li").mouseleave(function(){
	// 	$("header aside").removeClass("open");
	// 	$(".dropdown-row").removeClass("open");
	// 	$(".dropdown").removeClass(cat+"-bg open");		
	// });

	// $(".bottom-bar-header li").not(".more-link").mouseenter(function(){
	// 	cat = $(this).data("category");
	// 	$("header aside").addClass("open");
	// 	$(".dropdown").addClass(cat+"-bg open");
	// 	$(".dropdown-row[data-name~='"+cat+"']").addClass("open");
	// });	

	// $("header aside").mouseenter(function(){
	// 	$(this).addClass("open");
	// });

	// $(".bottom-bar-header li").mouseleave(function(){
	// 	$("header aside").removeClass("open");
	// 	$(".dropdown").removeClass("open");
	// });
});



