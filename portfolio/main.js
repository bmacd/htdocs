$(function(){

	var numOfProjs = $(".project").length;
	var projIndex = 0;
	var width = $(window).width();	

	//remove loading gif
	// $(window).on("load", function(){
	// 	$("main").removeClass("hidden");
	// 	setTimeout(function(){
	// 		$(".gif-loader").addClass("hidden");
	// 	}, 1600);
	// });

	// view toggle
	$(".view-btn").click(function(){
		$(this).toggleClass("full-view");
		if ($(".image-slider").hasClass("show")){
			switchToScrollView();
		}
		else{
			switchToSlideView();
		}
	});

	function switchToScrollView(){
		$(".image-slider, .slider-details").removeClass("show");
		$(".left-slide-arrow, .right-slide-arrow").addClass("hidden");
		setTimeout(function(){
			var currentRow = 0;
			for (var i =0; i<=(numOfProjs-1); i++){
				if(i % 2 === 0  && i!= 0){
					currentRow++;
				}

				var $removeProj = $(".project").eq(i);
				var $addProj = $(".scroll-row").eq(currentRow);
				$removeProj.detach().appendTo($addProj);

				var $remove = $(".project-info").eq(i);
				var $add = $(".project-info-cont").eq(i);
				$remove.detach().appendTo($add);
			}
			setTimeout(function(){
				$(".image-scroller").addClass("show");
			}, 300);
		}, 1200);
	};

	function switchToSlideView(){
		$(".image-scroller").removeClass("show");
		setTimeout(function(){
			$(".project").detach().appendTo(".image-slider");
			$(".project-info").detach().appendTo(".project-info-inner-cont");
			setTimeout(function(){
				$(".image-slider, .slider-details").addClass("show");
				$(".left-slide-arrow, .right-slide-arrow").removeClass("hidden");
			}, 300);
		}, 1200);
	};

	$(window).resize(function(){
		width = $(window).width();
		if (width<993){
			switchToSlideView();
		}		
	})

	//menu
	$(".menu-button").click(function(){
		$(this).toggleClass("active");
		$(".slide-menu").toggleClass("active");
		$("main").toggleClass("menu-active");
		if (width>991){
			$(".menu-overlay").toggleClass("active");
		}
	});	

	$(".menu-overlay").click(function(){
		if (width>991){
			$(this).removeClass("active");
			$(".menu-button").removeClass("active");
			$(".slide-menu").removeClass("active");
			$("main").removeClass("menu-active");
		}		
	});	

	//homepage slider	

	function sliderLength(){
		var projectWidth = 100/numOfProjs;
		$(".image-slider").css("width", numOfProjs+"00%");
		$(".project").css("width", projectWidth+"%");
	};

	sliderLength();

	$(".right-slide-arrow").unbind("click").click(function(event){

		var current = parseInt($(".project.active").data("id"));
		var direction = current + 1;		
		if(current != (numOfProjs - 1)){
			slider(current, direction);
		}
	});

	$(".left-slide-arrow").unbind("click").click(function(event){
		
		var current = parseInt($(".project.active").data("id"));
		var direction = current - 1;
		if(current != 0){
			slider(current, direction);
		}
	});

	var isMoving=false;

	$(".image-slider").on("mousewheel DOMMouseScroll MozMousePixelScroll", function(event, turn, delta) {
		width = $(window).width();
		if(width > 991){
			event.preventDefault();
		    if (isMoving) return;
		    isMoving = true;
		    if(event.originalEvent.wheelDelta/120 > 0) {
		    	//scrolling up (left)
	           $(".left-slide-arrow").eq(0).trigger("click");
	        }
	        else{
	        	//scrolling down (right)
	        	console.log("right");
	            $(".right-slide-arrow").eq(0).trigger("click");
	        }
	        setTimeout(function() {
		    	isMoving=false;
		    },2000);
		}
	    
	});

	function circleCount(){
		for (i=0; i<numOfProjs; i++){
			if (i == 0){
				$(".slide-circles ul").append("<li class='slide-circle active'></li>")
			}
			else{
				$(".slide-circles ul").append("<li class='slide-circle'></li>")
			}			
		}
	}

	circleCount();

	function slider(current, direction){
		$(".project").removeClass("active");
		$(".project").eq(direction).addClass("active");
		$(".image-slider").css("left", "-"+(direction)+"00%");
		$(".project-info").removeClass("active");
		$(".slide-circle").removeClass("active");
		$(".slide-circle:nth-child("+(direction+1)+")").addClass("active");
		setTimeout(function(){
			$(".project-info").removeClass("active");
			$(".project-info").eq(direction).addClass("active");
			$(".project-info").removeClass("type-trans");
			$(".project-info.active").addClass("type-trans");
		}, 1200)		
	};

	// TABLET

	$(".tablet-switch").click(function(){
		$(".left-column").toggleClass("show");
		$(".middle-column").toggleClass("show");
		$(".right-column").toggleClass("show");
		$(".menu-overlay").toggleClass("active");
	});

	//MOBILE
	$(".work-link").click(function(){
		$(".left-column").addClass("show");
		$(".right-column").addClass("show");
	});

	var touchY;
	var lastY;
	var lastX;
	var touching;

	$(".main-container").bind('touchstart', function (e){
		touchY = e.originalEvent.touches[0].clientY;
	});

	$(".main-container").bind('touchmove', function (e){
		e.preventDefault();
		currentY = e.originalEvent.touches[0].clientY;
		homeMobileScroll(lastY, currentY);
		lastY = currentY;
	});

	$(".image-slider").bind('touchstart', function (e){
		touching = true;
	});

	$(".image-slider").bind('touchend', function (e){
		touching = false;
	});

	$(".image-slider").bind('touchmove', function (e){
		currentX = e.originalEvent.touches[0].clientX;
		if(touching === true){
			if(lastX > currentX){
		       	$(".left-slide-arrow").eq(0).trigger("click");
		    }
		    else if(lastX < currentX){
		       	$(".right-slide-arrow").eq(0).trigger("click");
		    }
		    touching = false;
	    }
		lastX = currentX;		
	});

	function homeMobileScroll(startY, endY){
		if((touchY + 40) < startY){
	       	$(".left-column").removeClass("show");
			$(".right-column").removeClass("show");
	    }
	    else if((touchY + 40) > startY){
	       	$(".left-column").addClass("show");
			$(".right-column").addClass("show");
	    }
	}
});  

;(function(){
  function id(v){return document.getElementById(v); }
  function loadbar() {
    var ovrl = id("overlay"),
        prog = id("progress"),
        img = document.images,
        c = 0;
        tot = img.length;

    function imgLoaded(){
      c += 1;
      var perc = ((100/tot*c) << 0) +"%";
      prog.style.width = perc;
      if(c===tot) return doneLoading();
    }
    function doneLoading(){
      ovrl.style.opacity = 0;
      setTimeout(function(){ 
        ovrl.style.display = "none";
      }, 1200);
    }
    for(var i=0; i<tot; i++) {
      var tImg     = new Image();
      tImg.onload  = imgLoaded;
      tImg.onerror = imgLoaded;
      tImg.src     = img[i].src;
    }    
  }
  document.addEventListener('DOMContentLoaded', loadbar, false);
}());

