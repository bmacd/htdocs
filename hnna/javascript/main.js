$(function(){

	var width = $(window).width();

	//menu
	$(".hamburger-btn").click(function(){
		$(".menu").removeClass("hidden");
	});

	$(".close-menu").click(function(){
		$(".menu").addClass("hidden");
	});

	$(".menu").on("click", "a", function(){
		$(".menu").addClass("hidden");
	});

	//home page add lines for glitch text
	var origTitle = "HUMANS NEED NOT APPLY HUMANS NEED NOT APPLY";
	var origTitleArr = origTitle.split('');

	var glitchLines = 
	[
		"JoY&0P jÁZZ zZZ ZA7Zy PAZAZZ BUz™ VE× JoKE¥",
		"Z0MBI3 J0CK joY P1ZZa MuZZe| JE®K GOV FJæRD",
		"QUARKS HA©K BoX 7IPPY BO<>OW MoFo 33K D00Z*",
		"RoMAN£ RuLE POX QUACk 5CUNNY K00K SoZ MUMM¥",
		"WIZ4RD GA%% GyM CR@ZY PoMpOM CHÆF CUT FLUM?",
		"QWERTY DUN/ M4P BLOCK HoCkEY KN3W 4LL OXLIP",
		"Z£BRAS MUMS pUD K1NKY cOWBOy ©HIC H4G YACK$"
	];
	var lineCount = glitchLines.length;

	function terminalLines(){
		for (var i = 0; i<lineCount; i++){
			$(".glitch-text h2").append("<p data-num='"+i+"'></p>");
			for (var j = 0; j<origTitleArr.length; j++){
				if(j > (origTitleArr.length)/2){
					$(".glitch-text h2 p").eq(i).append("<span class='hidden-sm hidden-xs' data-num='"+j+"'>"+origTitleArr[j]+"</span>");
				}
				else{
					$(".glitch-text h2 p").eq(i).append("<span data-num='"+j+"'>"+origTitleArr[j]+"</span>");
				}
			}
		}
	}
	terminalLines();



	//home page glitch text 

	$(document).on("click", ".glitch-text h2 p span", function(){
		$(".glitch-text h2").html("");
		terminalLines();
	});

	$(document).on("mouseenter", ".glitch-text h2 p span", function(){
		var rowIndex = parseInt($(this).parent().data("num"));
		var spanIndex = parseInt($(this).data("num"))
		var newLine = glitchLines[rowIndex];
		var splitNewLine = newLine.split('');
		$(this).html(splitNewLine[spanIndex]);
		$(this).addClass("hovered");
	});


	var glitchCount = 0;
	var rowCount = 0;

	// $(document).on("mouseenter", ".glitch-text h2 p", function(){
	// 	$("#cursor").removeClass("hidden");
	// });

	// $(document).on("mousemove", ".glitch-text h2 p", function(e){
	// 	var offset = $(this).parent().offset();
	// 	$("#cursor").css("left", ((e.pageX - offset.left) + 10 )+"px");
	// 	$("#cursor").css("top", ((e.pageY - offset.top) + 10 )+"px");
	// });

	// $(document).on("mouseleave", ".glitch-text h2 p", function(){
	// 	$("#cursor").addClass("hidden");
	// });



	//video controls
	// $(".video-container").click(function(){
	// 	if (width > 993){
	// 		$(".video-overlay").removeClass("hidden")
	// 		$(".video-overlay video").get(0).play();
	// 	}	
	// });

	// $(".video-overlay video").click(function(){
	// 	$(this).toggleClass("paused");

	// 	if($(this).hasClass("paused")){
	// 		$(this).get(0).pause();
	// 	}
	// 	else{
	// 		$(this).get(0).play();	
	// 	}
	// });

	// $(".close-video").click(function(){
	// 	$(".video-overlay").addClass("hidden");
	// 	$(".video-overlay video").addClass("paused");
	// 	$(".video-overlay video").get(0).pause();
	// });

	//arrange exhibits
	function arrangeExhibs(){
		var exhibCount = $(".exhibit").length;		

		for (var i = 0; i < exhibCount; i++){
			var randomX = Math.floor(Math.random() * 100) + 1;
			var randomY = 0;
			if (i==0){
				randomY = 0;
			}
			else if (i==(exhibCount-1)){
				randomY = 97;	
			}
			else{
				randomY = Math.floor(Math.random() * 97) + 3;
			}
			var randomWidth = Math.floor(Math.random() * 25) + 15;

			$(".exhibit").eq(i).css("width", randomWidth+"%");

			//X-axis position
			if (randomX > 50){
				randomX = 100-randomX;
				$(".exhibit").eq(i).css("right", randomX+"%");
			}
			else{
				$(".exhibit").eq(i).css("left", randomX+"%");	
			}

			//Y-axis position
			if (randomY > 50){
				randomY = 100-randomY;
				$(".exhibit").eq(i).css("bottom", randomY+"%");
			}
			else{
				$(".exhibit").eq(i).css("top", randomY+"%");	
			}
			
		}
	}

	arrangeExhibs();

	//drag exhibitions
	var clicking = false;

	$(".exhibit").mouseenter(function(){
		$(".exhibit").removeClass("hovered");
		$(this).addClass("hovered");
	});

	$(".exhibit").click(function(){
		$(".exhibit").removeClass("hovered");
		$(this).addClass("hovered");
	});

	$(document).on("mousedown", ".exhibit.hovered img", function(e){
		e.preventDefault();
	    clicking = true;
	    currentPosX = $(this).parent().parent().position().left;
	    currentPosY = $(this).parent().parent().position().top;
	    mousePosX = e.pageX;
	    mousePosY = e.pageY;
	});

	$(document).mouseup(function(){
	    clicking = false;
	})

	$(document).on("mousemove", ".exhibit.hovered img", function(e){
		e.preventDefault();
	    if(clicking == false) return;
	    mousePosXMove = e.pageX;
	    mousePosYMove = e.pageY;
	    newPosLeft = currentPosX + (mousePosXMove - mousePosX);
	    newPosTop = currentPosY + (mousePosYMove - mousePosY);
	    $(this).parent().parent().css("left", newPosLeft+"px");
	    $(this).parent().parent().css("top", newPosTop+"px");
	});


	//newsletter signup
	$(".form-container input[type='email']").focus(function(){
		$(".form-container").addClass("focused");
	});

	//ticker text
	var tickerWidth = $(".quote-ticker h1").outerWidth();

	var ticker = function(targetElement, speed){

		$(targetElement).css({left:'100%'});
	    $(targetElement).animate(
	        {
	        'left': "-"+tickerWidth+'px'
	        },
	        {
	        duration: speed,
	        specialEasing: {
		      'left': 'linear'
		    },
	        complete: function(){
	        	// tickerCount++;
	            ticker(this, speed);
	            }
	        }
	    );

	};
	if (width>767){
		var speed = tickerWidth*1.1;
	}
	else{
		var speed = tickerWidth*4;
	}
	ticker($('.quote-ticker h1'), speed);

	//dropdown essays
	$(".page-dropdown .page-heading").click(function(){

		if($(this).parent().hasClass("open")){
			$(this).parent().removeClass("open");
			$(this).find(".open-icon").removeClass("hidden");
			$(this).find(".hide-icon").addClass("hidden");
		}
		else{
			$(this).parent().addClass("open");
			$(this).find(".open-icon").addClass("hidden");
			$(this).find(".hide-icon").removeClass("hidden");
		}		
	});



});