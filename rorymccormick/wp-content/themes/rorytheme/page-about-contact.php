<?php get_header();?>
<?php get_sidebar();?>

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php
				    $feat_image = wp_get_attachment_url( get_post_thumbnail_id() );
					echo "<img src = '".$feat_image."' alt = '".get_the_title()." Featured Image' />";
				?>				
			</div>
		</div>
		<div class="row page-desc">
			<div class="col-sm-3 hidden-xs">
				<h1>Hi!</h1>
			</div>
			<div class="col-sm-3">
				<h2>Say Hi!</h2>
				<?php 
					$value = get_field( "email" );
					if( $value ) {
						echo 
						"<h2 class='light-grey'>".$value."</h2>"
						;					    
					} else {
					    echo '';
					}
				?>	
			</div>
			<div class="col-sm-6">
				<?php 
					$value = get_field( "about_text" );
					if( $value ) {
						echo 
						"<p>".$value."</p>"
						;					    
					} else {
					    echo '';
					}
				?>	
				<div class="row">
					<?php 
						$value = get_field( "recognitions" );
						if( $value ) {
							echo 
							"							
							<div class='col-sm-6 special-col'>
								<h3>Recognition</h3>
								<p>".$value."</p>
							</div>"
							;					    
						} else {
						    echo '';
						}
					?>
	
					<?php 
						$value = get_field( "talks" );
						if( $value ) {
							echo 
							"							
							<div class='col-sm-6 special-col'>
								<h3>Talks</h3>
								<p>".$value."</p>
							</div>"
							;					    
						} else {
						    echo '';
						}
					?>
				</div>
			</div>
		</div>
	</div>
		
<?php get_footer();?>