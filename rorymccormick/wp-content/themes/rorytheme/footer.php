			</main>
			<footer class="light-grey">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<span>Rory McCormick © — Dev. Brendan McDonagh</span>
						</div>
						<div class="col-sm-6">	
							<span>
								<?php
									$page = get_page_by_path('about-contact');
									echo get_field('email', $page->ID);
								?>
							</span>
						</div>						
					</div>
				</div>
			</footer>
		</section>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> -->
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script type="text/javascript">
		var markerUrl = "<?php bloginfo('template_directory'); ?>/images/map-marker.png";
	</script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/map.js"></script>	
</html>