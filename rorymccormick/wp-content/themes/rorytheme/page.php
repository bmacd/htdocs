<?php get_header();?>
<?php get_sidebar();?>

	<div class="container">
		<div class="row page-desc">
			<div class="col-sm-6">
				<h1><?php echo get_the_title(); ?></h1>
				<?php 
					$value = get_field( "project_type" );
					if( $value ) {
						echo 
						"<h2 class='light-grey'>".$value."</h2>"
						;					    
					} else {
					    echo '';
					}
				?>	
			</div>
			<div class="col-sm-6 hidden-xs">
				<?php 
					$value = get_field( "project_description" );
					if( $value ) {
						echo 
						"<p>".$value."</p>"
						;					    
					} else {
					    echo '';
					}
				?>	
			</div>
		</div>
		<div class="row">	

			<?php 
				$value = get_field( "first_large_image" );
				if( $value ) {
					echo 
					"<div class='col-xs-12'>	
						<img src = '".$value."' alt = '".get_the_title()." Large Image One' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>	

			<?php 
				$value = get_field( "first_small_image" );
				if( $value ) {
					echo 
					"<div class='col-sm-6'>	
						<img src = '".$value."' alt = '".get_the_title()." Small Image One' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>	

			<?php 
				$value = get_field( "second_small_image" );
				if( $value ) {
					echo 
					"<div class='col-sm-6'>						
						<img src = '".$value."' alt = '".get_the_title()." Small Image Two' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>	

			<?php 
				$value = get_field( "second_large_image" );
				if( $value ) {
					echo 
					"<div class='col-xs-12'>	
						<img src = '".$value."' alt = '".get_the_title()." Large Image One' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>	

			<?php 
				$value = get_field( "third_small_image" );
				if( $value ) {
					echo 
					"<div class='col-sm-6'>	
						<img src = '".$value."' alt = '".get_the_title()." Small Image Three' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>	

			<?php 
				$value = get_field( "fourth_small_image" );
				if( $value ) {
					echo 
					"<div class='col-sm-6'>	
						<img src = '".$value."' alt = '".get_the_title()." Small Image Four' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>		

			<?php 
				$value = get_field( "fifth_small_image" );
				if( $value ) {
					echo 
					"<div class='col-sm-6'>						
						<img src = '".$value."' alt = '".get_the_title()." Small Image Five' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>	

			<?php 
				$value = get_field( "sixth_small_image" );
				if( $value ) {
					echo 
					"<div class='col-sm-6'>						
						<img src = '".$value."' alt = '".get_the_title()." Small Image Six' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>	

			<?php 
				$value = get_field( "third_large_image" );
				if( $value ) {
					echo 
					"<div class='col-xs-12'>	
						<img src = '".$value."' alt = '".get_the_title()." Second Image One' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>

			<?php 
				$value = get_field( "seventh_small_image" );
				if( $value ) {
					echo 
					"<div class='col-sm-6'>						
						<img src = '".$value."' alt = '".get_the_title()." Small Image Seven' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>

			<?php 
				$value = get_field( "eighth_small_image" );
				if( $value ) {
					echo 
					"<div class='col-sm-6'>						
						<img src = '".$value."' alt = '".get_the_title()." Small Image Eighth' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>

			<?php 
				$value = get_field( "fourth_large_image" );
				if( $value ) {
					echo 
					"<div class='col-xs-12'>						
						<img src = '".$value."' alt = '".get_the_title()." Large Image Three' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>

			<?php 
				$value = get_field( "fifth_large_image" );
				if( $value ) {
					echo 
					"<div class='col-xs-12'>
						<img src = '".$value."' alt = '".get_the_title()." Large Image Four' />
					</div>"
					;					    
				} else {
				    echo '';
				}
			?>

		</div>
	</div>
		
<?php get_footer();?>