			<?php get_header();?>
			<?php get_sidebar();?>
			<div class="container">
				<div class="row">
					<?php
						$pages = get_posts(array(
							'post_type'      => 'page',
							'post_status'    => 'publish',
							'orderby'		 => 'menu_order',
							'order'		     => 'ASC',
							'posts_per_page' => -1
						));	
						foreach ($pages as $page) {
							$page_cats = get_the_category($page->ID);					
							if ($page_cats[0]->slug == "project"){
								$src_array = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), 'optional-size' );
    							$src = $src_array[0];

							    echo 
							    "<div class='col-sm-6 home-project-container'>
								    <a href='".get_page_link($page->ID)."''>
								    	<img src = '".$src."' alt = '".$page->post_title."' />
								    	<h2>".$page->post_title."</h2>
								    	<h3 class='light-grey'>".get_field('project_type', $page->ID)."</h3>
								    </a>
							    </div>"
							    ;	
							}
						};	
					?>
				</div>
			</div>
			
			<?php get_footer();?>
