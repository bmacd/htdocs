<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">    
	    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	    
	    <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet">
	    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bootstrap/css/bootstrap.min.css" type="text/css" media="screen" />
	    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">	    
	</head>
	<body>

			<header>
				<div class="container">
					<div class="row">
						<div class="col-sm-6 logo">
							<?php
								$home_url = home_url().'/';
							?>
							<a href="<?php echo $home_url; ?>">
								<span>Rory McCormick</span>
								<span class="light-grey">Design and Art Direction</span>
							</a>							
						</div>
						<div class="col-sm-6 nav">
							<ul>
								<li>									
									<a href="<?php echo $home_url; ?>">
										Projects
									</a>
								</li>
								<?php
									$page = get_page_by_path('about-contact');
									echo '<li><a href="'.get_permalink( $page->ID ).'">'.$page->post_title.'</a></li>';
								?>
							</ul>
						</div>
					</div>
				</div>
			</header>
			
			<main class="main-container">