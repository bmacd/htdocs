<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'rorymccormick');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b_WYvxFCl>|$V9^at%5_gnZWX]sDLv]}@8}D5v-j#~_D4n_h,>RLh5,h--)SU92{');
define('SECURE_AUTH_KEY',  's^wbmaQu X:x0zJ`VGM2a`SO%vf+)3qc!N_D;jquWZ+PS;u$G=(z8{):ECx<y7&o');
define('LOGGED_IN_KEY',    'bDaB=lGEw6R/b0i(7gP%wx~K>5I44|>)2|*?ZX8Lpj0NBHalLH2hQ(`]4N!!@=#(');
define('NONCE_KEY',        'eOEN>)L!B/qoww9PBWtdz0v=}_U=/ZP&O^r2El3Try4B(U>F?DqW_K]*^DuQFfiY');
define('AUTH_SALT',        'poJ5,&<|54}WKC$4en84B[vNLu+s*djw.~gT}&Gh14wF|&Jvpi)2ciF(E0(RgEDj');
define('SECURE_AUTH_SALT', 'JMJe{cuzcS=#U+{2i(+eGaZQ#55g$P1TQe|dr< L^$,Tqw$i_6Byg>{j ,e+CM$<');
define('LOGGED_IN_SALT',   'YwHUb,?O7o(VB6~R%.C$aC[ly&#<}M/!S5FYc/xkfq911;QW8[tSVl.9dfp+xc%2');
define('NONCE_SALT',       ')pjej_Aj`-bUxw{ff&YGhi_<F#n7.Ju}k B%r*8ZASr|f5r!Y*Eq Zeh>~0*t#Jm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
