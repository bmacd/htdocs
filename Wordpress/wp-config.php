<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tj-|aS6HNx|2M`Q]vs6m.+ZK$$k#*=0R~:ICW;Uq |uJ_!$ yXHmFXIz**gUENz%');
define('SECURE_AUTH_KEY',  '{;sB%|r7YEuS1XZI~-1l ko@r&_t]U{|NCvlqdrstA]RQ3f!~zD?jfu,>2=$L=^i');
define('LOGGED_IN_KEY',    '!+dNE&:;niy$Q{d/1:[P~~FZA@EhV%pb![XxqPvCD9_~kO{x5uW0lXgY`6ebzA;5');
define('NONCE_KEY',        'W+.iqkFHmdtC!jsLj*m@bMG!K><AANROH.=9.6v)IqcsV~KZ7:/vIi`oc+4b/8g=');
define('AUTH_SALT',        '1U/{oMB6kOBpm8{`D@.dZmW}CKdpEV_-GLM+<^JDB-H[Q,;AA-oYI<YfSR9+-,_V');
define('SECURE_AUTH_SALT', 'Vi%  J]L$,=[GOlhs3<s[Aac6T@~Z8>~OZ@HciAsbi83.y5:eA2Rz1n0^:D-0}+(');
define('LOGGED_IN_SALT',   'Fm$m*GPrnam]O}|~9@cr<(Z7D~k~Z6>Ke7>glKT0 k{BH0R]Ha?@~BS^n0(q;pUV');
define('NONCE_SALT',       'UKVfAL$/xw^&*X9Nic2 x61){l+A8{2b3|Y~6!7(slnn<]RUER[}M/mgXwXF740X');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
