	<footer>
				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<p>If you have any questions or would like to book an appointment with Bea contact her by phone on <span class="bold">353 (0) 87 915 7384</span> or send an email to <span class="bold">info@beahealthynutrition.ie</span></p>
						</div>
						<div class="col-md-6 col-md-offset-1">
							<div class="row">
								<div class="col-md-4">
									<ul>
										<?php wp_list_pages('title_li=&number=2'); ?>
									</ul>
								</div>
								<div class="col-md-4">
									<ul>
										<?php wp_list_pages('title_li=&number=2&offset=2'); ?>
									</ul>
								</div><div class="col-md-4">
									<ul>
										<?php wp_list_pages('title_li=&number=2&offset=4'); ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</main>		
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/map.js"></script>	
</html>