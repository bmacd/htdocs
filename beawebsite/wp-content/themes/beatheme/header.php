<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	    
	    <link rel="stylesheet" href="<? bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">
		<link href='https://fonts.googleapis.com/css?family=Lato:400, 700' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a href="<?php echo home_url(); ?>/" class="logo">Bea Healthy Nutrition</a>						
						<nav class="desktop-nav hidden-xs hidden-sm">
							<ul>
								<?php wp_list_pages('title_li='); ?>
							</ul>
						</nav>
						<div class="hamburg-menu visible-xs visible-sm">
							<span class="bar"></span>
							<span class="bar"></span>
							<span class="bar"></span>
						</div>
					</div>
				</div>
			</div>
		</header>
		<aside class="side-menu">
			<nav class="mobile-nav visible-xs visible-sm">
				<ul>
					<?php wp_list_pages('title_li='); ?>
					<li class="break"></li>
					<li class="social">info@beahealthynutrition.ie</li>
					<li class="social">@beahealthy</li>					
				</ul>
			</nav>
		</aside>
		<main class="main-container">