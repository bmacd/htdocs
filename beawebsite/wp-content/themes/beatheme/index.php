			<?php get_header();?>
			<?php get_sidebar();?>
			<section class="main-home-image">
				<div class="main-home-text">					
					<img src="<?php bloginfo('template_directory'); ?>/images/home-text.svg" alt="Beacause your health is important" />
					<br />
					<a href="contact" class="box-link">Book Consultation</a>
				</div>
			</section>

			<section class="about-container container">
				<h2>Who is Bea?</h2>
				<div class="row">				
					<div class="col-sm-6 col-md-5 about-image"></div>
					<div class="col-sm-6 col-md-offset-1 about-text">
						<p>I am Beatrix Szabo, one of the few Nutritional Therapists in Ireland who holds a BSc in Nutritional Medicine. On this site I would like to introduce you to the basic principals of Nutritional Therapy. You can find out what happens during a consultation and what you can do prior to the consultation to optimize our time together. Also, I would like to share my personal experience of the power of Nutritional Medicine and my philosophy of how good food is the fuel of Life</p>
						<a href="contact" class="box-link">Book Consultation</a>
						<div class="row qualifications">
							<div class="col-sm-6">
								<h4>CERTIFIED NUTRITIONIST</h4>
								<p class="small">
									BSc(Hons) in Nutritional Medicine
								</p>
							</div>
							<div class="col-sm-6">
								<h4>PRACTICES LATEST TRENDS</h4>
								<p class="small">
									Bea stays ahead of consumer demand and uses the latest and most excisting trends
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>			

			<section class="testimonial-container">
				<div class="container">
					<h2>Testimonials</h2>
					<div class="row">									
						<div class="col-md-10 col-md-offset-1">
							<h3>I was a self confessed sweet junkie and after finishing the program I had no more cravings... Thanks Bea for saving my life..</h3>
							<span class="quoter">ANIKO SMITH</span>						
						</div>
					</div>
				</div>
			</section>

			<section class="ethos-container">
				<div class="container">
					<h2>Ethos</h2>
					<div class="row">									
						<div class="col-md-10 col-md-offset-1">
							<h3>Good nutrition is the foundation for a happy, healthy and energetic you!</h3>
							<p>I view the human body as the most sophisticated “machine/computer” on the planet. Therefore it needs a very unique and complex fuel to function at its optimum level. If it doesn’t get its right fuel in the form of an adequate intake of protein, carbohydrates, essential fatty acids, vitamins, minerals and antioxidants it will malfunction; just like a car that is filled up with coca-cola instead of petrol. Without the right fuel = food the human body slowly loses its ability to produce energy and to heal itself. Therefore one of the crucial parts of my job is to assess and address your individual needs to achieve your goals.</p>
							<!-- <a href="ethos.html" class="box-link">Learn More</a> -->
						</div>
					</div>
				</div>
			</section>
			
			<?php get_footer();?>

