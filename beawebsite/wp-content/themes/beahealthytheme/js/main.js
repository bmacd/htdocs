$(function(){

	$(".hamburg-menu").click(function(){
		$(".side-menu").toggleClass("open");
		$(this).toggleClass("clicked");
	});

	$(".services-dropdown-link").click(function(){
		$(this).toggleClass("clicked");
		$(this).find(".arrow-down").toggleClass("clicked");
		$(".services-dropdown").toggleClass("hidden");
	})

	$(".more-link").click(function(){
		var height = $(window).height();
		$("html, body").animate({ scrollTop: height }, 600);
	});

	var loop=0;
	var slides = $(".main-home-image").length;	
	$(".main-home-slider").css("width", slides+"00vw");
	$(".right-arrow").click(function(){		
		$(".left-arrow").removeClass("no-slides");
		if(loop > (slides-2)){
			$(".right-arrow").addClass("no-slides");
		}
		else{			
			loop++;	
			$(".main-home-slider").css("left", "-"+loop+"00vw");
			$(".right-arrow").removeClass("no-slides");
		}		
	});

	$(".left-arrow").click(function(){	
		$(".right-arrow").removeClass("no-slides");		
		if(loop < 1)	{
			$(".left-arrow").addClass("no-slides");
		}
		else{			
			loop--;
			$(".main-home-slider").css("left", "-"+loop+"00vw");
			$(".left-arrow").removeClass("no-slides");
		}		
	});
});