<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta property="og:url" content="http://beahealthynutrition.ie" />
		<meta property="og:type" content="site" />
		<meta property="og:title" content="Bea Healthy Nutrition" />
		<meta property="og:description" content="I am Beatrix Szabo, one of the few Nutritional Therapists in Ireland who holds a BSc in Nutritional Medicine. On this site I would like to introduce you to the basic principals of Nutritional Therapy. You can find out what happens during a consultation and what you can do prior to the consultation to optimize our time together. Also, I would like to share my personal experience of the power of Nutritional Medicine and my philosophy of how good food is the fuel of Life" />
		<meta property="og:image" content="<?php bloginfo('template_directory'); ?>/images/banner-image-test.jpeg" />
	    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	    
	    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">
		<link href='https://fonts.googleapis.com/css?family=Lato:400, 700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/font-awesome/css/font-awesome.min.css" type="text/css" media="screen" />
	</head>
	<body>
		<header>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<a href="<?php echo home_url(); ?>/" class="logo">Bea Healthy Nutrition</a>						
						<nav class="desktop-nav hidden-xs hidden-sm">
							<ul>
								<li class="services-dropdown-link">
									Services <span class="arrow-down"></span>
									<ul class="services-dropdown hidden">
										<?php
											$main_links = get_posts(array(
												'category'		=> 6,
												'post_type'	    => 'page',
												'post_status'	=> 'publish',
												'orderby'		=> 'menu_order',
												'order'         => 'ASC'
											));
											foreach($main_links as $link){
												echo '<li><a href="'.get_page_link($link->ID).'">'.$link->post_title.'</a></li>';
											}
										?>
									</ul>
								</li>
								<?php
									$main_links = get_posts(array(
										'category'		=> 7,
										'post_type'	=> 'page',
										'post_status'	=> 'publish',
										'orderby'		=> 'menu_order',
										'order'         => 'ASC'
									));
									foreach($main_links as $link){
										echo '<li><a href="'.get_page_link($link->ID).'">'.$link->post_title.'</a></li>';
									}
								?>
								<li style="font-size: 1.2em;">
									<a href="https://www.facebook.com/Bea-Healthy-Nutrition-588340404514266/" target="_blank">
										<i class="fa fa-facebook-official"></i>
									</a>
								</li>
							</ul>							
						</nav>
						<div class="hamburg-menu visible-xs visible-sm">
							<span class="bar"></span>
							<span class="bar"></span>
							<span class="bar"></span>
						</div>
					</div>
				</div>
			</div>
		</header>
		<aside class="side-menu">
			<nav class="mobile-nav visible-xs visible-sm">
				<ul>	
					<?php
						$main_links = get_posts(array(
							'category'		=> 6,
							'post_type'	=> 'page',
							'post_status'	=> 'publish',
							'orderby'		=> 'menu_order'
						));
						foreach($main_links as $link){
							echo '<li><a href="'.get_page_link($link->ID).'">'.$link->post_title.'</a></li>';
						}
					?>
					<li class="break"></li>				
					<?php
						$main_links = get_posts(array(
							'category'		=> 7,
							'post_type'	=> 'page',
							'post_status'	=> 'publish',
							'orderby'		=> 'menu_order'
						));
						foreach($main_links as $link){
							echo '<li><a href="'.get_page_link($link->ID).'">'.$link->post_title.'</a></li>';
						}
					?>
					<li class="break"></li>
					<li class="social">info@beahealthynutrition.ie</li>
					<li class="social"><a href="https://www.facebook.com/Bea-Healthy-Nutrition-588340404514266/">Facebook</a></li>					
				</ul>
			</nav>
		</aside>
		<main class="main-container">			