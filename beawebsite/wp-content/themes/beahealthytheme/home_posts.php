<section class="posts-container">
	<div class="container">
		<h2>Recent Posts & Recipes</h2>
		<div class="row">									
			<div class="col-md-3">							
				<?php
					$args = array( 'numberposts' => '1' );
					$recent_posts = wp_get_recent_posts($args);
					foreach( $recent_posts as $recent ){
						$feat_image = wp_get_attachment_url( get_post_thumbnail_id($recent->ID) );									

						echo '<a href="' . get_permalink($recent["ID"]) . '">
							<div class="post post-1" style="background-image: url(' . $feat_image . ')">
							</div>
						</a> 
						';
					}
				?>
			</div>
			<div class="col-md-3">
				<?php
					$args = array( 'numberposts' => '1', 'offset' => '1' );
					$recent_posts2 = wp_get_recent_posts($args);
					foreach( $recent_posts2 as $recent ){
						$feat_image = wp_get_attachment_url( get_post_thumbnail_id($recent->ID) );									

						echo '<a href="' . get_permalink($recent["ID"]) . '">
							<div class="post post-2" style="background-image: url(' . $feat_image . ')">
							</div>
						</a> 
						';
					}
				?>
				<?php
					$args = array( 'numberposts' => '1', 'offset' => '2' );
					$recent_posts = wp_get_recent_posts($args);
					foreach( $recent_posts as $recent ){
						$feat_image = wp_get_attachment_url( get_post_thumbnail_id($recent->ID) );									

						echo '<a href="' . get_permalink($recent["ID"]) . '">
							<div class="post post-3" style="background-image: url(' . $feat_image . ')">
							</div>
						</a> 
						';
					}
				?>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<div class="post post-4">
							<div class="twitter-info">
								<p>Join Bea on Twitter for more updates and healthy advice!</p>
								<a href="">@beahealthy</a>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<?php
							$args = array( 'numberposts' => '1', 'offset' => '3' );
							$recent_posts = wp_get_recent_posts($args);
							foreach( $recent_posts as $recent ){
								$feat_image = wp_get_attachment_url( get_post_thumbnail_id($recent->ID) );									

								echo '<a href="' . get_permalink($recent["ID"]) . '">
									<div class="post post-5" style="background-image: url(' . $feat_image . ')">
									</div>
								</a> 
								';
							}
						?>
					</div>
					<div class="col-md-12">
						<?php
							$args = array( 'numberposts' => '1', 'offset' => '4' );
							$recent_posts = wp_get_recent_posts($args);
							foreach( $recent_posts as $recent ){
								$feat_image = wp_get_attachment_url( get_post_thumbnail_id($recent->ID) );									

								echo '<a href="' . get_permalink($recent["ID"]) . '">
									<div class="post post-6" style="background-image: url(' . $feat_image . ')">
									</div>
								</a> 
								';
							}
						?>
					</div>
				</div>
			</div>
		</div>
		<!-- <a class="box-link">
			load more
		</a> -->
	</div>
</section>