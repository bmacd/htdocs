			<?php get_header();?>
			<?php get_sidebar();?>
			<section class="main-home-slider-container">
				<div class="main-home-slider">
					<div class="main-home-image first-slide">
						<div class="main-home-text">					
							<h1>Welcome to Bea’s Nutritional Medicine and Bioresonance Mini-Clinic</h1>
						</div>
					</div>
					<?php
						$pages = get_posts(array(
							'category'		=> 6,
							'post_type'	=> 'page',
							'post_status'	=> 'publish',
							'orderby'		=> 'menu_order',
							'order'         => 'ASC'
						));
						foreach($pages as $page){
							$feat_image = wp_get_attachment_url( get_post_thumbnail_id($page->ID) );
							echo 							
							'<a class="slide-link" href="'.get_page_link($page->ID).'">
								<div class="main-home-image" style="background-image: url('.$feat_image.')">
									<div class="slide-main-header">
										<h2>SERVICES</h2>
										<h3>'.$page->post_title.'</h3>
									</div>	
								</div>
							</a>';
						}
					?>				
				</div>
				<div class="more-link">
					<span class="more-text">MORE</span>
					<img class="more-arrow" src="<?php bloginfo('template_directory'); ?>/images/more-arrow.png">
				</div>
				<img class="left-arrow no-slides hidden-xs" src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png">
				<img class="right-arrow hidden-xs" src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png">
			</section>

			<section class="about-container container">
				<h2>Who is Beatrix?</h2>
				<div class="row">				
					<div class="col-sm-6 col-md-5 about-image"></div>
					<div class="col-sm-6 col-md-offset-1 about-text">
						<p>
I am Beatrix Szabo, one of the few nutritionists in Ireland holding a BSc in Nutritional Medicine.  In my practice I bring together traditional natural medicine, up to date research and modern technology.  
I aim to give the best possible care to my clients therefore both therapies, nutritional and bioresonance, are tailor made with regard to the individual's unique genetic and biochemical fingerprint.  Preferences, background, budget work/family schedule are all taken into consideration before the outline of any therapy plan.</p>
						<a href="index.php/contact/" class="box-link">Book Consultation</a>
						<div class="row qualifications">
							<div class="col-sm-6">
								<h4>CERTIFIED NUTRITIONIST</h4>
								<p class="small">
									BSc(Hons) in Nutritional Medicine
									<br />
									Bioresonance Therapist
									<br />
									Nutri Clean Program Practitioner
									<br />
									<a href="http://irishtherapists.ie/content/archti-irish-therapists-archti-online-payments"><img src="<?php bloginfo('template_directory'); ?>/images/archti.jpg" style="display: inline-block; height: 20px;"> Member of Archti</a>
								</p>								
							</div>
							<div class="col-sm-6">
								<h4 style="text-transform: uppercase; margin: 2.5em;"></h4>
								<p class="small">									
									Beatrix is committed to continuous professional development, keeps up to date with latest health-science research. Only working with the best quality supplements on the market.
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>			

			<section class="testimonial-container">
				<div class="container">
					<h2>Testimonials</h2>
					<div class="row">									
						<div class="col-md-10 col-md-offset-1">
							<h3>I was a self confessed sweet junkie and after finishing the program I had no more cravings... Thanks Bea for saving my life..</h3>
							<span class="quoter">ANIKO SMITH</span>						
						</div>
					</div>
				</div>
			</section>

			<section class="ethos-container">
				<div class="container">
					<h2>Ethos</h2>
					<div class="row">									
						<div class="col-md-10 col-md-offset-1">
							<h3>Good Nutrition is the Foundation of a Happy, Healthy and Energetic YOU!</h3>
							<p>I view the human body as the most sophisticated “engine” on the planet. Therefore it needs a very unique and complex fuel to function at its optimum level. If it doesn’t get its right fuel in the form of an adequate intake of protein, carbohydrates, essential fatty acids, vitamins, minerals and antioxidants it will malfunction; just like a car that is filled up with coca-cola instead of petrol. Without the right fuel = food the human body slowly loses its ability to produce energy and to heal itself. I experienced the power of Nutritional Medicine myself. From the age of 14 I was suffering from debilitating eczema flare ups on my hand. No medication or cream was able to bring long term relief. It all changed when I started to learn about nutrition. I improved my diet and started taking supplements. Finally, I’m in control eczema doesn’t control me.</p>
							<!-- <a href="ethos.html" class="box-link">Learn More</a> -->
						</div>
					</div>
				</div>
			</section>
			
			<?php get_footer();?>

