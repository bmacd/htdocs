<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PrA~8#w8Rc!yK9^?5N1G61P8)FX;E;t{!@<xeA6o*RdY405k4il]1^`60C%is +3');
define('SECURE_AUTH_KEY',  '/T#<fp[i|G<@kc(y7z-|$@cbXj9L[(_7F%vHW%~(dXPFG7IB$%?&BPfFWKp1JTT=');
define('LOGGED_IN_KEY',    'Rhn==AKYorbGHx&iY$O/A7Wq|<AEDHi8pQUIygfI^Wfuw<o30hj~[2Io*~F!7MY2');
define('NONCE_KEY',        'Ty6rDl!Xt_6:!gOwq){3?@cx!(;zRg%&Cngbq~O=14Z8 B]=IVSx@b(O3%43faiM');
define('AUTH_SALT',        '1los _V9?X^X0|C4_#wk.O%/;gt=KS=TQSbwc:MDna}TExt|5Ou?(eU067ch{N,+');
define('SECURE_AUTH_SALT', '%vh6h0}oxXR-qEd=**:9WE&lO0h=y^q.QX|6PZ3p:x0.pcPUxx{pwDL}DZf,]~wp');
define('LOGGED_IN_SALT',   'oovcVr< Ga@Hn]Uyjg,X@</P@%|02!_[2+ot:2.q;Jnz~i$_ JC.~`GHYzwlb~fq');
define('NONCE_SALT',       '46a)PF}[AEDT$m1gB^zi`!jfE-@Ltal#)jaBtB|>$_&tf/6e}&]l=9I(1<MA=D)<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
