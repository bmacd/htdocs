$(function() {
    // slider menu
    $(".menu-icon").click(function(){
    	$(".mobile-menu").addClass("open");
    });
    $(".close-menu-icon").click(function(){
    	$(".mobile-menu").removeClass("open");
    });

    //header scroll
	$(window).scroll(function(){
		sliderHeight = $(".product-slider").outerHeight() - 50;
		sliderAboutHeight = $(".product-slider").outerHeight() + $(".about-delta-container") .outerHeight();

	    if ($(window).scrollTop() >= sliderHeight){
	        $("header").removeClass("hide-header");
	    }
	    else {
	    	$("header").addClass("hide-header");
	    }

	});

	//disable header links
	$('.dropdown-link, .mobile-dropdown-link').click(function(event){
	    event.preventDefault();
	}); 

	//show desktop dropdown

	$('.dropdown-link').hover(
	  function() {
	    $(this).css("background", "#fff");
	    $(this).css("color", "#157efb");
	  }, function() {
	    $(this).css("background", "transparent");
	    $(this).css("color", "#fff");
	  }
	); 

	$(".desktop-menu li").hover(
	  function() {
	    $(this).find("ul").css("display", "block");
	  }, function() {
	    $(this).find("ul").css("display", "none");
	  }
	);

	$(".desktop-dropdown-menu").hover(
	  function() {
	    $(this).parent().find(".dropdown-link").css("background", "#fff");
	    $(this).parent().find(".dropdown-link").css("color", "#157efb");
	  }, function() {
	    $(this).parent().find(".dropdown-link").css("background", "transparent");
	    $(this).parent().find(".dropdown-link").css("color", "#fff");
	  }
	);

	//show mobile dropdown

	$(".mobile-dropdown-link").click(function(){
		$(".mobile-dropdown-menu").removeClass("open");
		$(this).parent().find(".mobile-dropdown-menu").addClass("open");
	});

	//image slider
	var slides=$(".slide");
	var numOfSlides=$(".slide").length;
	var currentElement = new Array();

	$(".product-slider").css("width", numOfSlides + "00vw");
	var loop = 1;

	for (var i=0; i<numOfSlides; i++){
		$(".slide-links ul").append('<li class="slide-to-link slide-to-link-'+ i +'"><i class="fa fa-circle-o"></i></li>');
		$(".slide-to-link-0").html("<i class='fa fa-circle'></i>");
		currentElement.push(i); 
	}

	var slideLoop = setInterval(function () {
		if(!(loop == numOfSlides)){
			$(".slide-to-link").html("<i class='fa fa-circle-o'></i>");
			$(".product-slider").css("left", "calc(-"+loop+"00vw)");
			$(".slide-to-link-"+loop).html("<i class='fa fa-circle'></i>");
			loop++;
		}

		else{
			loop = 1;
			$(".slide-to-link").html("<i class='fa fa-circle-o'></i>");
			$(".product-slider").css("left", "0");
			$(".slide-to-link-"+(loop-1)).html("<i class='fa fa-circle'></i>");
		}
	}, 4000);

	$(currentElement).each(function(index, item){
		$(".slide-to-link-" + item).click(function(){ 
			$(".slide-to-link").html("<i class='fa fa-circle-o'></i>");
			$(".product-slider").css("left", "calc(-"+item+"00vw)");
			$(".slide-to-link-" + item).html("<i class='fa fa-circle'></i>");
			loop=item+1;
			clearInterval(slideLoop);
		});    
	});

	function swipeLeft(){
		if(!(loop == numOfSlides)){
			$(".slide-to-link").html("<i class='fa fa-circle-o'></i>");
			$(".product-slider").css("left", "calc(-"+loop+"00vw)");
			$(".slide-to-link-"+loop).html("<i class='fa fa-circle'></i>");
			loop++;
		}

		else{
			loop = 1;
			$(".slide-to-link").html("<i class='fa fa-circle-o'></i>");
			$(".product-slider").css("left", "0");
			$(".slide-to-link-"+(loop-1)).html("<i class='fa fa-circle'></i>");
		}
		clearInterval(slideLoop);
	}

	function swipeRight(){
		if(!(loop == 1)){
			$(".slide-to-link").html("<i class='fa fa-circle-o'></i>");
			$(".product-slider").css("left", "calc(-"+(loop-2)+"00vw)");
			$(".slide-to-link-"+(loop-2)).html("<i class='fa fa-circle'></i>");
			loop--;
		}

		else{
			loop = numOfSlides;
			$(".slide-to-link").html("<i class='fa fa-circle-o'></i>");
			$(".product-slider").css("left", "calc(-"+(loop-1)+"00vw)");
			$(".slide-to-link-"+(loop-1)).html("<i class='fa fa-circle'></i>");
		}
		clearInterval(slideLoop);
	}

	$(".product-slider").on("swipeleft",function(){
		swipeLeft();
	}); 

	$(".product-slider").on("swiperight",function(){
		swipeRight();
	});

	$(".product-dropdown").click(function(){		
		if($(this).hasClass("clicked")){
			$(this).removeClass("clicked");			
			$(this).find("span").html("+");
		}
		else{
			$(".product-dropdown").removeClass("clicked");
			$(this).addClass("clicked");
			$(".product-dropdown").find("span").html("+");
			$(this).find("span").html("–");
		}
	});
});