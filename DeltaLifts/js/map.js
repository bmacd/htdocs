function initialize() {
    var myLatlng = new google.maps.LatLng(53.319289, -6.360859);

    var mapOptions = {
      zoom: 17,
      center: myLatlng,
      scrollwheel: false,
      draggable: false,
      styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
    }

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(53.1743346,-6.8096257),
      map: map,
      title: 'John P Delaney',
      icon: markerUrl,
      url: 'https://www.google.ie/maps/place/John+P.+Delaney+Architects+Ltd/@53.1743346,-6.8096257,17z/data=!3m1!4b1!4m5!3m4!1s0x485d7fc29b714775:0x790f740e941525ec!8m2!3d53.1743314!4d-6.8074317'
    });
    
    var map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
  }

  google.maps.event.addDomListener(window, 'load', initialize);