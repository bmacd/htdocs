<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Delta Mobility - Contact Us</title>

	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Lato:400' rel='stylesheet' type='text/css'>
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<header>
			<a class="home-link" href="index.html">
				<img class="delta-logo" src="images/logo-text.png" alt="Delta Mobility">
			</a>
			<div class="menu-icon hidden-md hidden-lg">
				<i class="fa fa-bars"></i>
			</div>
			<nav>
				<ul class="desktop-menu hidden-sm hidden-xs">
					<li>
						<a class="dropdown-link" href="#">Stairlifts</a>
						<ul class="desktop-dropdown-menu">
							<li><a href="straightDomestic.html">Straight Domestic Stairlift</a></li>
							<li><a href="outdoorDomestic.html">Outdoor Domestic Stairlift</a></li>
							<li><a href="wheelchairStraight.html">Wheelchair Straight Stairlift</a></li>
							<li><a href="wheelchairCurved.html">Wheelchair Curved Stairlift</a></li>
						</ul>
					</li>
					<li>
						<a class="dropdown-link" href="#">Domestic Lifts</a>
						<ul class="desktop-dropdown-menu">
							<li><a href="domesticThrough.html">Domestic Through-Floor Lift</a></li>
							<li><a href="domesticPassenger.html">Domestic Passenger Lift</a></li>
						</ul>
					</li>
					<li>
						<a class="dropdown-link" href="#">Disabled Access</a>
						<ul class="desktop-dropdown-menu">
							<li><a href="verticalOneMeter.html">Vertical Stairlift up to 1m</a></li>
							<li><a href="verticalTwoMeter.html">Vertical Stairlift up to 2m</a></li>
						</ul>
					</li>
					<li>
						<a class="dropdown-link" href="#">Specialty</a>
						<ul class="desktop-dropdown-menu">
							<li><a href="heavyGoods.html">Heavy Goods Lifts</a></li>
							<li><a href="industrialSize.html">Industrial Sized Lifts</a></li>
						</ul>
					</li>
					<li>
						<a href="contact.php">Contact</a>
						<!-- <ul class="desktop-dropdown-menu contact">
							<li><span class="bold-text">E: </span>brendan@deltamobility.ie</li>
							<li><span class="bold-text">T: </span>086 - 256 4249</li>
						</ul> -->
					</li>
				</ul>
			</nav>
		</header>


		<!-- Slide out menu for mobile -->
		<aside class="mobile-menu hidden-md hidden-lg">
			<div class="close-menu-icon hidden-md hidden-lg">
				<i class="fa fa-close"></i>
			</div>
			<ul class="main-list">
				<li class="main-link">
					<a class="mobile-dropdown-link" href="#">Stairlifts</a>
					<ul class="mobile-dropdown-menu">
						<li><a href="straightDomestic.html">Straight Domestic Stairlift</a></li>
						<li><a href="outdoorDomestic.html">Outdoor Domestic Stairlift</a></li>
						<li><a href="wheelchairStraight.html">Wheelchair Straight Stairlift</a></li>
						<li><a href="wheelchairCurved.html">Wheelchair Curved Stairlift</a></li>
					</ul>
				</li>
				<li class="main-link">
					<a class="mobile-dropdown-link" href="#">Domestic Lifts</a>
					<ul class="mobile-dropdown-menu">
						<li><a href="domesticThrough.html">Domestic Through-Floor Lift</a></li>
						<li><a href="domesticPassenger.html">Domestic Passenger Lift</a></li>
					</ul>
				</li>
				<li class="main-link">
					<a class="mobile-dropdown-link" href="#">Disabled Access</a>
					<ul class="mobile-dropdown-menu">
						<li><a href="verticalOneMeter.html">Vertical Stairlift up to 1m</a></li>
						<li><a href="verticalTwoMeter.html">Vertical Stairlift up to 2m</a></li>
					</ul>
				</li>
				<li class="main-link">
					<a class="mobile-dropdown-link" href="#">Specialty</a>
					<ul class="mobile-dropdown-menu">
						<li><a href="heavyGoods.html">Heavy Goods Lifts</a></li>
						<li><a href="industrialSize.html">Industrial Sized Lifts</a></li>
					</ul>
				</li>
				<li class="main-link">
					<a href="contact.php">Contact</a>
				</li>
				<li class="mobile-contact-details">
					<span class="bold-text">E: </span>brendan@deltamobility.ie
					<br>
					<span class="bold-text">T: </span>086 - 256 4249
				</li>
			</ul>
		</aside>

		<!-- Main Container -->
		<main class="main-container">
			<div id="mapCanvas"></div>
			<div class="container contact-container">
				<div class="row">
					<div class="col-sm-6">
						<p class="contact-about hidden-xs">Delta Mobility is a nationwide Irish company which has over 30 years of experience installing, repairing and maintaining mobility aids. Since the company started in 1985 more than 2000 happy customers have put their trust in the Delta Mobility team. We hope you do the same. </p>
						<div class="row" style="margin-top: 5em;">
							<div class="col-sm-6">
								<label>ADDRESS</label>
								<ul>
									<li>Delta Lifts Limited,</li>
									<li>Bridges Industrial Estate,</li>
									<li>Red Cow,</li>
									<li>Naas Road,</li>
									<li>Dublin 22</li>
								</ul>
							</div>
							<div class="col-sm-6">
								<label>CONTACT</label>
								<ul>
									<li>Phone - 086 256 4249</li>
									<li>Email - deltamobility@gmail.com</li>
								</ul>
							</div>
						</div>
						<div class="row" style="margin-top: 5em;">	
							<div class="col-xs-12">
								<label>BUSINESS ENQUIRIES</label>
								<p>The Through-floor Lift is a modern and unobtrusive solution providing access over 2 floors. Designed using state-of-the-art lift technology, the Through-floor Lift maximises the space available to you; when you are downstairs, the lift can be moved quietly and discreetly back upstairs</p>
							</div>
						</div>
					</div>
					<div class="col-sm-5 col-sm-offset-1">
						<?php 
						$action=$_REQUEST['action']; 
						if ($action=="")    /* display the contact form */ 
						    { 
						    ?> 
						    <form  action="" method="POST" enctype="multipart/form-data"> 
						    <input type="hidden" name="action" value="submit"> 
						    <label for="name">NAME</label>
						    <input name="name" type="text" value=""/><br> 
						    <label for="email">EMAIL</label>
						    <input name="email" type="text" value=""/><br> 
						    <label for="message">MESSAGE</label>
						    <textarea name="message"></textarea><br> 
						    <input type="submit" value="SEND"/> 
						    </form> 
						    <?php 
						    }  
						else                /* send the submitted data */ 
						    { 
						    $name=$_REQUEST['name']; 
						    $email=$_REQUEST['email']; 
						    $message=$_REQUEST['message']; 
						    if (($name=="")||($email=="")||($message=="")) 
						        { 
						        echo "All fields are required, please fill te form again."; 
						        } 
						    else{         
						        $from="From: $name<$email>\r\nReturn-path: $email"; 
						        $subject="E-mail from contact form"; 
						        mail("macdonagh.brendan@gmail.com", $subject, $message, $from); 
						        echo "Email sent"; 
						        } 
						    }   
						?>
					</div>
				</div>
			</div>
		</main>

		<!-- Footer -->

		<footer>
			<div class="container">
				<div class="row email-row">					
					<div class="col-xs-12">
						<p class="large"><a href="contact.php">Click here</a> to get in touch or call Brendan on <span class="tel-number">086 - 256 4249</span></p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<ul>
							<li><h4>Stairlifts</h4></li>
							<li><a href="straightDomestic.html">Straight Domestic Stairlift</a></li>
							<li><a href="outdoorDomestic.html">Outdoor Domestic Stairlift</a></li>
							<li><a href="wheelchairStraight.html">Wheelchair Straight Stairlift</a></li>
							<li><a href="wheelchairCurved.html">Wheelchair Curved Stairlift</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<ul>
							<li><h4>Stairlifts</h4></li>
							<li><a href="domesticThrough.html">Domestic Through-Floor Lift</a></li>
							<li><a href="domesticPassenger.html">Domestic Passenger Lift</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<ul>
							<li><h4>Disabled Access</h4></li>
							<li><a href="verticalOneMeter.html">Vertical Stairlift up to 1m</a></li>
							<li><a href="verticalTwoMeter.html">Vertical Stairlift up to 2m</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<ul>
							<li><h4>Specialty Lifts</h4></li>
							<li><a href="heavyGoods.html">Heavy Goods Lifts</a></li>
							<li><a href="industrialSize.html">Industrial Sized Lifts</a></li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> -->
	<script type="text/javascript" src="js/main.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDDNESViO8xRJj7Zz5vB8jSa5Jkph05CHo"></script>
	<script type="text/javascript">
		var markerUrl = "images/map-marker.png";
	</script>
	<script type="text/javascript" src="js/map.js"></script>
</html>

