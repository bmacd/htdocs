$(function() {
	var loopImages = 0;
	var slideCount = $(".image-div").length;

	$(".total-number").html(slideCount);

	//logo change text

	function changeLogoText(){
		var width = $(window).width();
		if (width > 768){
			$(".logo a").html("John P Delaney Architects Limited");
		}
		else{
			$(".logo a").html("JPD");
		}
	}

	changeLogoText();

	$(window).resize(function(){	
		changeLogoText();
	});

	$(".right-arrow-hover, .mobile-right-arrow").click(function(){
		$(".image-div").removeClass("current");
		$(".slide-info .image-title").removeClass("current");
		if(loopImages < slideCount-1){			
			loopImages++;			
			setTimeout(function(){
				$(".image-div").eq(loopImages).addClass("current");
				$(".slide-info .image-title").eq(loopImages).addClass("current");
				$(".current-number").html(loopImages+1);
			}, 400);
		}	
		else{
			loopImages = 0;			
			setTimeout(function(){
				$(".image-div").eq(0).addClass("current");
				$(".slide-info .image-title").eq(0).addClass("current");
				$(".current-number").html(loopImages+1);				
			}, 400);
		}	
	});

	$(".left-arrow-hover, .mobile-left-arrow").click(function(){
		$(".image-div").removeClass("current");
		$(".slide-info .image-title").removeClass("current");
		if(loopImages == 0){
			loopImages = slideCount -1;
			setTimeout(function(){
				$(".image-div").eq(loopImages).addClass("current");
				$(".slide-info .image-title").eq(loopImages).addClass("current");
				$(".current-number").html(loopImages + 1);
			}, 400);
		}	
		else{
			loopImages --;			
			setTimeout(function(){
				$(".image-div").eq(loopImages).addClass("current");
				$(".slide-info .image-title").eq(loopImages).addClass("current");
				$(".current-number").html(loopImages + 1);				
			}, 400);
		}	
	});

	$(".hamburger-menu").click(function(){
		setTimeout(function(){
			$(".body-container").addClass("open");
			$(".body-overlay").addClass("open");
			$(".menu").addClass("open");
			$(".mobile-slider-div").css("right", "100%");
		}, 100);		
	});	

	$('body').on('click', '.body-container.open, .close-menu', function() {
	    $(".body-container").removeClass("open");
		$(".body-overlay").removeClass("open");
		$(".menu").removeClass("open");
		$(".mobile-slider-div").css("right", "0");
	});

	$(".cat-dropdown-link").click(function(){
		$(this).toggleClass("open");
		$(".cat-dropdown-menu").toggleClass("open");
	});

	//homepage scroll

	var scrollLengthLeft = 0;
	var scrollLengthRight = 0;

	$(window).on("scroll", function(){
		if($('.left-home-project').length > 0){
			var scrollTop     = $(window).scrollTop(),
		        elementOffset = $('.left-home-project').eq(scrollLengthLeft).offset().top,
		        distance      = (elementOffset - scrollTop),
		        length        = $('.left-home-project').length,
		        height        = $(window).height() * 0.7;	        
	        
	        if (distance < height ){
	        	if(scrollLengthLeft != (length -1)){
	        		$('.left-home-project').eq(scrollLengthLeft).addClass("scrolled");
	        		scrollLengthLeft ++;
	        	}
	        	else{
	        		$('.left-home-project').eq(scrollLengthLeft).addClass("scrolled");
	        	}
	        }
		}		
	});

	$(window).on("scroll", function(){
		if($('.right-home-project').length > 0){
			var scrollTop     = $(window).scrollTop(),
		        elementOffset = $('.right-home-project').eq(scrollLengthRight).offset().top,
		        distance      = (elementOffset - scrollTop),
		        length        = $('.right-home-project').length,
		        height        = $(window).height() * 0.7;	        
	        
	        if (distance < height){        	
	        	if (scrollLengthRight != (length - 1)){
	        		$('.right-home-project').eq(scrollLengthRight).addClass("scrolled");
		        	scrollLengthRight++;
		        }
		        else{
		        	$('.right-home-project').eq(scrollLengthRight).addClass("scrolled");
		        }
	        }
	    }
	});

	//change image position of larger	
	// $(window).load(function(){
	// 	$(".page-small-images-row").each(function(){
	// 		firstImage = $(this).find(".small-image:first-of-type img");
	// 		secondImage = $(this).find(".small-image:nth-of-type(2) img");
	// 		if(firstImage.outerHeight() > secondImage.outerHeight()){
	// 			secondImage.addClass("smaller");
	// 		}
	// 		else if(firstImage.outerHeight() < secondImage.outerHeight()){
	// 			firstImage.addClass("smaller");
	// 		}
	// 	});		
	// });

	//about page scroll
	$('.about-filters a').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  headerHeight = $("header").outerHeight();
		  if (target.length) {
		    $('html, body').animate({
		      scrollTop: target.offset().top - headerHeight
		    }, 1000);
		    return false;
		  }
		}
	});
});