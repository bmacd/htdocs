<?php get_header();?>
<?php get_sidebar();?>

<section class="page-title-container container">	
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">			
			<h1><a class='red' href='<?php bloginfo('url'); ?>/category/news'>News</a></h1>
		</div>
		<div class="col-md-6 col-sm-8 page-sub-title">
			<h3><?php the_title(); ?></h3>
			<h4><?php echo get_the_date( 'd/m/Y' ); ?></h4>
			<p><?php echo get_field( "first_paragraph" ); ?></p>
		</div>
	</div>
</section>				

<section class="page-small-images container">
	<div class="row">	
		<?php 
		$image = get_field('small_image_one');
		if( !empty($image) ): ?>
			<div class="col-sm-6 small-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>
		<?php 
		$image = get_field('small_image_two');
		if( !empty($image) ): ?>
			<div class="col-sm-6 small-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>				
	</div>					
</section>
<?php
	$paragraph = get_field('second_paragraph', $post->ID);
	if( !empty($paragraph) ){
		echo'
		<section class="page-copy container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 page-info-container">
					<p>'.$paragraph.'</p>
				</div>	
			</div>
		</section>
		';
	}
?>

<?php
	$image = get_field('first_large_image', $post->ID);
	if( !empty($image) ){
		echo '
		<section class="page-image container" style="background-image:url('.$image.')"></section>'
		;	
	}
?>

<?php
	$image = get_field('second_large_image', $post->ID);
	if( !empty($image) ){		
		echo '
		<section class="page-image container" style="background-image:url('.$image.')"></section>'
		;	
	}
?>

<?php
	$paragraph = get_field('third_paragraph', $post->ID);
	if( !empty($paragraph) ){
		echo'
		<section class="page-copy container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 page-info-container">
					<p>'.$paragraph.'</p>
				</div>	
			</div>
		</section>
		';
	}
?>	

<?php
	$image = get_field('third_large_image', $post->ID);
	if( !empty($image) ){
		echo '
		<section class="page-image container" style="background-image:url('.$image.')"></section>'
		;	
	}
?>

<?php get_footer();?>