<?php get_header();?>
<?php get_sidebar();?>

<section class="page-title-container container">	
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h2 class="red">
				<?php 
					$id = get_the_ID();
					$page_cats = get_the_category($id);	
					
					if ($page_cats[0]->slug == "projects"){
						$parcat = $page_cats[0]->term_id;
						$category_name = $page_cats[0]->cat_name;						
						$category_link = get_category_link( $parcat );	
					}
					else{
						$parcat = $page_cats[1]->term_id;
						$category_name = $page_cats[1]->cat_name;
						$category_link = get_category_link( $parcat );
					}
	 				echo "<a class='red' href='".esc_url( $category_link )."'>".$category_name."</a>";
				?>
			</h2>			
		</div>
		<div class="col-md-6 col-sm-4 page-sub-title">
			<h1><?php the_title(); ?></h1>
		</div>
		<div class="col-md-3 col-sm-4 share-links">
			<ul>
				<li>Share</li>
				<li>
					<a href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank">
				        <i class="fa fa-facebook"></i>
				    </a>
			    </li>
			    <li>
				    <a href="https://twitter.com/share?url=<?php echo get_permalink(); ?>&amp;text=<?php echo 'JPD Architects - '.get_the_title(); ?>&amp;hashtags=jpdarchitects" target="_blank">
				        <i class="fa fa-twitter"></i>
				    </a>
			    </li>			    
			</ul>
		</div>
	</div>
</section>

<?php
	$feat_image = get_field('page_main_image', $page->ID);
	echo '
	<section class="page-image container" style="background-image:url('.$feat_image.')"></section>'
	;		
?>

<section class="page-copy container">
	<div class="row">
		<div class="col-sm-6 page-info-container">			
			<?php 
				$value = get_field( "client" );
				if( $value ) {
					echo 
					'<div class="page-info">
						<h4 class="title">Client</h4>
						<h4>'.$value.'</h4>
					</div>'
					;					    
				} else {
				    echo '';
				}
			?>	
			<?php 
				$categories = get_the_category();
				if( ! empty( $categories) ) {
					$value = get_field( "project_type" );
					if( $value ) {
						echo 
						'<div class="page-info">
							<h4 class="title">Project Type</h4>
							<h4>'.$value.'</h4>
						</div>'
						;
					}
					else{
						echo 
						'<div class="page-info">
							<h4 class="title">Project Type</h4>
							<h4>'.$categories[0]->name.'</h4>
						</div>'
						;	
					}
				} else {
				    echo '';
				}
			?>	
			<?php 
				$value = get_field( "location" );
				if( $value ) {
					echo 
					'<div class="page-info">
						<h4 class="title">Location</h4>
						<h4>'.$value.'</h4>
					</div>'
					;					    
				} else {
				    echo '';
				}
			?>	
			<?php 
				$value = get_field( "project_value" );
				if( $value ) {
					echo 
					'<div class="page-info">
						<h4 class="title">Project Value</h4>
						<h4>'.$value.'</h4>
					</div>'
					;					    
				} else {
				    echo '';
				}
			?>							
		</div>
		<div class="col-sm-6 page-text">
			<?php 
				$value = get_field( "first_paragraph_content" );
				if( $value ) {
				    echo $value;
				} else {
				    echo '';
				}
			?>
		</div>
	</div>
</section>
<section class="page-small-images container">
	<div class="row page-small-images-row">
		<?php 
		$image = get_field('small_image_one');
		if( !empty($image) ): ?>
			<div class="col-sm-6 small-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>
		<?php 
		$image = get_field('small_image_two');
		if( !empty($image) ): ?>
			<div class="col-sm-6 small-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>
	</div>
	<div class="row page-small-images-row">
		<?php 
		$image = get_field('small_image_three');
		if( !empty($image) ): ?>
			<div class="col-sm-6 small-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>
		<?php 
		$image = get_field('small_image_four');
		if( !empty($image) ): ?>
			<div class="col-sm-6 small-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>
	</div>
	<div class="row page-small-images-row">
		<?php 
		$image = get_field('small_image_five');
		if( !empty($image) ): ?>
			<div class="col-sm-6 small-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>
		<?php 
		$image = get_field('small_image_six');
		if( !empty($image) ): ?>
			<div class="col-sm-6 small-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>
	</div>
	<div class="row page-small-images-row">
		<?php 
		$image = get_field('large_image');
		if( !empty($image) ): ?>
			<div class="col-sm-12 large-image" style="background-image:url('<?php echo $image; ?>')"></div>
		<?php endif; ?>
	</div>
</section>
<section class="similar-projects-container container">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="red">Similar Projects</h2>
		</div>
		<div class="clearfix"></div>
		<?php
			$id = get_the_ID();
			$page_cats = get_the_category($id);	
			
			if ($page_cats[0]->slug != "projects"){
				$childcat = $page_cats[0]->term_id;	
			}
			else{
				$childcat = $page_cats[1]->term_id;				
			}
			$pages = get_posts(array(
				'category'		 => $childcat,
				'post_type'	     => 'page',
				'post_status'	 => 'publish',
				'orderby'		 => 'rand',	
				'posts_per_page' => 4,
				'post__not_in'   => array($post->ID)
			));
			foreach($pages as $page){
				$feat_image = get_field('page_main_image', $page->ID);
				$page_cats = get_the_category( $page->ID );
				if ($page_cats[0]->slug != "projects"){
					$childcat_name = $page_cats[0]->cat_name;
				}
				else{
					$childcat_name = $page_cats[1]->cat_name;
				}				
				echo 
				'<a href="'.get_page_link($page->ID).'">
					<div class="col-md-3 col-sm-6 similar-project" style="background-image: url('.$feat_image.')">
						<div class="project-overlay">
							<div class="project-title">
								'.$page->post_title.'
								<span class="category">
									'.$childcat_name.'
								</span>
							</div>
						</div>
					</div>
				</a>'		
				;				
			}
		?>
	</div>
</section>
<?php get_footer();?>