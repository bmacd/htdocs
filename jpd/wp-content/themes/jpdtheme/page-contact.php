<?php get_header();?>
<?php get_sidebar();?>
<section class="page-title-container container">	
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h1 class="about-title red">Contact</h1>
		</div>
		<div class="col-md-9 col-sm-8">
			<div class="row">
				<div class="col-md-6">
					<ul>
						<li>Moorefield Business Centre</li>
						<li>Moorefield Road, Newbridge</li>
						<li>Co.Kildare, W12 P382</li>
						<li>Ireland</li>
						<li><br /></li>
						<li>T +353 (0)45 431 011</li>						
						<li>info@jpdarchitects.ie</li>
					</ul>
				</div>								
			</div>
		</div>
	</div>
</section>	

<section class="page-title-container container">	
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h2 class="about-title red">Social Media</h2>
		</div>
		<div class="col-md-9 col-sm-8">
			<div class="row">
				<div class="col-md-4">
					<ul>						
						<li><a href="https://www.facebook.com/jpdarchitects/">Facebook</a></li>
						<li><a href="https://www.linkedin.com/company/john-p-delaney-architects">LinkedIn</a></li>
						<li><a href="https://www.instagram.com/jpdarchitects/">Instagram</a></li>
						<li class="visible-sm"><br /></li>						
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>				

<section class="page-image container">
	<div id="mapCanvas" class="col-xs-12"></div>
</section>	
<?php get_footer();?>