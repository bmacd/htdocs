<?php get_header();?>
<?php get_sidebar();?>

<section class="page-title-container container">	
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h1 class="red">News</h1>
		</div>
	</div>
</section>
<section class="projects-list container">
	<div class="row">
		<?php
			$pages = get_posts(array(				
				'post_type'	=> 'post',
				'post_status'	=> 'publish',
				'orderby' => 'date',
				'order' => 'DESC'
			));
			foreach($pages as $page){
				$feat_image = get_field('first_large_image', $page->ID);
				$feat_text = get_field('first_paragraph', $page->ID);
				echo 
				'<a href="'.get_permalink($page->ID).'">
					<div class="col-md-4 col-sm-6">
						<div class="project" style="background-image: url('.$feat_image.')">
						</div>
						<div class="news-info">
							<h3 class="news-title">
								'.$page->post_title.'
							</h3>
							<h4 class="news-date">
								'.date('d/m/Y', strtotime($page->post_date)).'
							</h4>
							<article class="news-short-para">
								'.substr($feat_text,0,100).'...
							</article>
						</div>
					</div>
				</a>'				
				;
			}
		?>	
	</div>
</section>	

<?php get_footer();?>