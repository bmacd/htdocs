<?php get_header();?>
<?php get_sidebar();?>
<section class="page-title-container container hidden-xs">	
	<div class="page-title-row row">
		<div class="col-xs-12 page-title about-filters">
			<h2 class="about-title red"><a href="#about"><?php the_title(); ?></a></h2>
			<h2 class="about-title"><a href="#people">People</a></h2>
			<h2 class="about-title"><a href="#services">Services</a></h2>
			<h2 class="about-title"><a href="#clients">Clients</a></h2>
			<h2 class="about-title"><a href="#careers">Careers</a></h2>
		</div>		
	</div>
</section>
<section class="page-title-container container" id="about">
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h1 class="about-title red"><?php the_title(); ?></h1>			
		</div>
		<div class="col-md-6 col-sm-8 page-sub-title">							
			<p>
				The qualities that you look for in good design you will find in us. Uncompromising standards,technological innovation, fresh imagination. That’s why we have been a leading architectural firm for over forty years. Our experience tells.
				<br />
				<br />
				Our ethos is simple and sound. Each client, build and site is unique and requires us to think uniquely.Each project determines its own design solution. We can only be happy when you are. 
				<br />
				<br />
				Our practice is a team effort. Without teamwork, design will not work. The founding principles of JPD guide all of our activities. We work hard. We talk. We listen and we care. Only then can we do our job properly. 
				<br />
				<br />
				Professional buildings need a professional approach. Good design. Good practice.
			</p>
		</div>
	</div>
</section>		

<!-- <section class="page-title-container container" id="people">
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h2 class="about-title red">People</h2>			
		</div>
		<div class="col-md-9 col-sm-8 page-sub-title">							
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<img src="<?php bloginfo('template_directory'); ?>/images/people-placeholder.jpg">
					<span class="person">Tim Tomothy</span>
					<span class="role">Lorem</span>
				</div>
				<div class="col-md-4 col-sm-6">
					<img src="<?php bloginfo('template_directory'); ?>/images/people-placeholder.jpg">
					<span class="person">Tim Tomothy</span>
					<span class="role">Lorem</span>
				</div>
				<div class="col-md-4 col-sm-6">
					<img src="<?php bloginfo('template_directory'); ?>/images/people-placeholder.jpg">
					<span class="person">Tim Tomothy</span>
					<span class="role">Lorem</span>
				</div>
				<div class="col-md-4 col-sm-6">
					<img src="<?php bloginfo('template_directory'); ?>/images/people-placeholder.jpg">
					<span class="person">Tim Tomothy</span>
					<span class="role">Lorem</span>
				</div>
				<div class="col-md-4 col-sm-6">
					<img src="<?php bloginfo('template_directory'); ?>/images/people-placeholder.jpg">
					<span class="person">Tim Tomothy</span>
					<span class="role">Lorem</span>
				</div>
				<div class="col-md-4 col-sm-6">
					<img src="<?php bloginfo('template_directory'); ?>/images/people-placeholder.jpg">
					<span class="person">Tim Tomothy</span>
					<span class="role">Lorem</span>
				</div>
			</div>
		</div>
	</div>
</section>	 -->

<section class="container" id="people">
	<img src="<?php bloginfo('template_directory'); ?>/images/office-profile-image.jpg" />	
</section>	

<section class="page-title-container container" id="services">
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h2 class="about-title red">Services</h2>			
		</div>
		<div class="col-md-6 col-sm-8 page-sub-title">							
			<p>
				Architectural Design
				<br />
				Project Management
				<br />
				Commercial and Residential Interior Design
				<br />
				Architectural Conservation Grade 3
				<br />
				Conciliation and Arbitration Service,
				<br />
				BER Certificates and Green/Sustainable Design
				<br />
				Planning Advice and Consultancy
				<br />
				Assigned Certifier and Building Control Consultancy
				<br />
				Commercial / Retail Fit-Outs
			</p>
		</div>
	</div>
</section>				

<section class="page-image container">
	<img src="<?php bloginfo('template_directory'); ?>/images/studio-image.jpg">
</section>	

<section class="page-title-container container" id="clients">	
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h2 class="about-title red">Clients</h2>
		</div>
		<div class="col-md-9 col-sm-8 clients-list">
			<div class="row">
				<div class="col-md-4">
					<ul>
						<li>Allied Irish Bank</li>
						<li>Aquascutum</li>
						<li>Armani</li>
						<li>Asics</li>
						<li>Brown Bag Films</li>
						<li>Camphill Communities of Ireland</li>
						<li>Department of Education</li>
						<li>Eastern Health Board</li>
						<li>Falke</li>
						<li>Fossil</li>
						<li>Godolphin Ireland</li>
						<li>Health Service Executive</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul>
						<li>Hickey’s Pharmacy Group</li>
						<li>K.A.R.E.</li>
						<li>Kalu Emporium</li>
						<li>Kildare County Council</li>
						<li>Kilkenny Design Centre</li>
						<li>Killcullen GAA</li>
						<li>Le Pain Quotidien</li>
						<li>Lidl UK Assets GmbH</li>
						<li>Mothercare</li>
						<li>Napapijri</li>
						<li>Newbridge College</li>
						<li>Newbridge Credit Union</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul>
						<li>Newbridge Silverware</li>
						<li>Penhaligons</li>
						<li>Ralph Lauren</li>
						<li>St. Vincent De Paul</li>
						<li>Swarovski</li>
						<li>The Department of Defence</li>
						<li>The Keadeen Hotel</li>
						<li>Timberland</li>
						<li>Travelodge Ireland</li>
						<li>Under Armour</li>
						<li>Value Retail Dublin Ltd.</li>
						<li>Versace</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>	
<section class="page-title-container container" id="careers">
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<h2 class="about-title red">Careers</h2>			
		</div>
		<div class="col-md-6 col-sm-8 page-sub-title">							
			<p>
				John P Delaney Architects is an award winning design led practice with 
				a wide and varied range of exciting new projects. We are not seeking 
				people at the moment. However, if you are enthusiastic and passionate 
				about design and feel that you would like to work with us at some time 
				in the future, we’d love to hear from you. Please submit your CV by 
				email to <span class="red">info@jpdarchitects.ie</span>
			</p>
		</div>
	</div>
</section>
<?php get_footer();?>
