<?php get_header();?>
<?php get_sidebar();?>

<section class="page-title-container container">	
	<div class="page-title-row row">
		<div class="col-md-3 col-sm-4 page-title">
			<?php 
			  $idObj = get_category_by_slug('projects'); 
			  $id = $idObj->term_id;
			  $link = get_category_link( $id );
			  echo'
				<h1 class="red">
					<a class="red" href = "'.$link.'"">Projects</a>
				</h1>
			  ';
			?>			
		</div>
		<div class="col-md-9 col-sm-8 page-sub-title cat-dropdown-link">
			<h2>
				Categories&nbsp;
				<?php
					$categories = get_the_category();
 					$idObj = get_category_by_slug('projects'); 
					
					foreach((get_the_category()) as $childcat) {					
						if (cat_is_ancestor_of($idObj, $cat)) {					
							$childbool = true;							
						}
						if ($childcat->slug != "projects"){
							$childcat_name = $childcat->cat_name;	
						}
					}

					if ( ! empty( $categories ) ) {
						if ($childbool === true){
					    	echo '<div style="color: #a7a7a7; display:inline-block;">('.esc_html( $childcat_name ).')</div>';  					    	
					    } 
					} 
				?> 
				<span class="dropdown-arrow visible-sm visible-xs"><img src="<?php bloginfo('template_directory'); ?>/images/down-arrow.png"></span>
			</h2>
		</div>
	</div>
	<div class="cat-dropdown-menu row">			
				<? 						
					$i = 1;
					$med = 3;
					$max = 6;	
					$cat = get_query_var('cat');
  					$maincat = get_category ($cat);
					$slug = $maincat->slug;	
					if ($slug == "projects") {						
						$childcategories = get_categories('child_of=' . $cat . '&hide_empty=1');	
					}
					else{												
						$catPar = get_category_by_slug("projects");
						$catParId = $catPar->term_id;
						$childcategories = get_categories('child_of=' . $catParId . '&hide_empty=1');
					}					
					echo'<div class="col-sm-4"><ul>';
						foreach ($childcategories as $childcategory) {						
						  if (in_category($childcategory)) {
						    echo '<li><h3><a href="';
						    bloginfo('url');
						    echo '/category/'.$cat_id->category_nicename.'/'.$childcategory->category_nicename.'">';
						    echo $childcategory->cat_name . '</a></h3>';
						    echo '</li>';
						  }
						  if ($i++ == 3){
						  	$i = 1;
						  	break;
						  } 
						}
					echo'</ul></div>';					
					echo'<div class="col-sm-4"><ul>';
						foreach ($childcategories as $childcategory) {						
						  if (in_category($childcategory && $i > $med)) {
						    echo '<li><h3><a href="';
						    bloginfo('url');
						    echo '/category/'.$cat_id->category_nicename.'/'.$childcategory->category_nicename.'">';
						    echo $childcategory->cat_name . '</a></h3>';
						    echo '</li>';
						  }
						  if ($i++ == 6){
						  	$i = 1;
						  	break;
						  } 
						}
					echo'</ul></div>';

					echo'<div class="col-sm-4"><ul>';
						foreach ($childcategories as $childcategory) {						
						  if (in_category($childcategory && $i > $max)) {
						    echo '<li><h3><a href="';
						    bloginfo('url');
						    echo '/category/'.$cat_id->category_nicename.'/'.$childcategory->category_nicename.'">';
						    echo $childcategory->cat_name . '</a></h3>';
						    echo '</li>';
						  }
						  $i++;
						}
					echo'</ul></div>';
				?>	
		</div>		
	</div>
</section>
<section class="projects-list container">
	<div class="row">
		<?php
			$maincat = get_category ($cat);
			$slug = $maincat->slug;

			if ($slug == "projects") {							
				$pages = get_posts(array(
					'category'		 => $cat,
					'post_type'	     => 'page',
					'post_status'	 => 'publish',
					'orderby'		 => 'title',
					'order'          => 'ASC',
					'tag'            => 'top-of-page-projects',
					'posts_per_page' => -1
				));
			}			
			else{
				$pages = get_posts(array(
					'category'		 => $cat,
					'post_type'	     => 'page',
					'post_status'	 => 'publish',
					'orderby'		 => 'menu_order',
					'order'          => 'ASC',					
					'posts_per_page' => -1
				));
			}			
			foreach($pages as $page){				
				$feat_image = get_field('page_main_image', $page->ID);
				$page_cats = get_the_category( $page->ID );
				$idObj = get_category_by_slug('projects'); 

				foreach((get_the_category($page->ID)) as $childcat) {
					if (cat_is_ancestor_of($idObj, $childcat)) {
						$childcat_name = $childcat->cat_name;	
					}
				}

				echo 
				'<a href="'.get_page_link($page->ID).'">
					<div class="col-md-4 col-sm-6 project" style="background-image: url('.$feat_image.')">
						<div class="project-overlay">
							<div class="project-title">
								'.$page->post_title.'
								<span class="category">
								'.$childcat_name.'
								</span>								
							</div>
						</div>
					</div>
				</a>'		
				;
			}
		?>
		<?php
			$pages = get_posts(array(
				'category'		 => $cat,
				'post_type'	     => 'page',
				'post_status'	 => 'publish',
				'orderby'		 => 'menu_order',	
				'order'		     => 'ASC',
				'posts_per_page' => -1
			));
			$maincat = get_category ($cat);
			$slug = $maincat->slug;
			if ($slug != "projects") {							
				$rootCat = true;
			}			
			else{
				$rootCat = false;
			}
			foreach($pages as $page){
				if ( (!has_tag('top-of-page-projects', $page)) && !$rootCat ){
					$feat_image = get_field('page_main_image', $page->ID);
					$page_cats = get_the_category( $page->ID );
					$idObj = get_category_by_slug('projects'); 

					foreach((get_the_category($page->ID)) as $childcat) {
						if (cat_is_ancestor_of($idObj, $childcat)) {
							$childcat_name = $childcat->cat_name;	
						}
					}

					echo 					
					'<a href="'.get_page_link($page->ID).'">
						<div class="col-md-4 col-sm-6 project" style="background-image: url('.$feat_image.')">
							<div class="project-overlay">
								<div class="project-title">
									'.$page->post_title.'
									<span class="category">
									'.$childcat_name.'
									</span>
								</div>
							</div>
						</div>
					</a>'		
					;
				}
			}
		?>		
	</div>
</section>
<?php get_footer();?>