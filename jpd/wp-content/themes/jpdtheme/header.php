<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <?php
	    	if (is_single()){
	    		$og_image = get_field('first_large_image', $post->ID);
	    		$og_type = "article";
	    		$og_title = "John P Delaney Architects - News(".get_the_title().")";
	    		$og_desc = get_field('first_paragraph', $post->ID);
	    	}
	    	else if (is_page()){
	    		$og_image = get_field('page_main_image', $page->ID);
	    		$og_type = "website";
	    		$og_title = "John P Delaney Architects - Project(".get_the_title().")";
	    		$og_desc = get_field('first_paragraph_content', $post->ID);
	    	}
	    	else{
	    		$og_image = "http://jpdarchitects.ie/wp-content/uploads/2016/08/Dominican-College-JPD-John-P-Delaney-Architects.jpg";
	    		$og_type = "website";
	    		$og_title = "John P Delaney Architects";
	    		$og_desc = "John P Delaney Architects Ltd is an Irish based multidisciplinary practice. With over forty years experience in the planning and design of large and medium scale projects, our knowledge and understanding of architectural place.";
	    	}
	    	$og_url = basename(get_permalink());
	    	echo '
	    	<meta property="og:image" content="'.$og_image.'" />
	    	<meta property="og:url" content="'.$og_url.'" />
			<meta property="og:type" content="'.$og_type.'" />
			<meta property="og:title" content="'.$og_title.'" />
			<meta property="og:description" content="'.$og_desc.'" />
	    	'
	    	;
	    ?>	    
	    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

	    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/font-awesome/css/font-awesome.min.css" type="text/css" media="screen" />
	    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen">	    
	</head>
	<body>
		<?php
			if ( is_home() ) {				
				echo
				'<div class="mobile-slider-div visible-xs visible-sm">
					<span class="mobile-left-arrow">
						<img src="'.get_bloginfo("template_directory").'/images/left-arrow.png">
					</span>
					<span class="mobile-right-arrow">
						<img src="'.get_bloginfo("template_directory").'/images/right-arrow.png">
					</span>
				</div>'
				;
			}
		?>		
		<aside class="menu">
			<div class="menu-inner-cont row">
				<?php
					$home_url = home_url().'/';
				?>			
				<div class="menu-logo"><a href="<?php echo $home_url; ?>">JPD</a></div>
				<div class="close-menu">
					<?php
						echo '<img src="'.get_bloginfo("template_directory").'/images/close-bars.svg">';
					?>				
				</div>
				<div class="close-slide-out hidden-xs">
					<span>Close</span>
				</div>
			</div>
			<ul class="navigation">				
				<?php
					$page = get_page_by_path('about');
					echo '<li><a href="'.get_permalink( $page->ID ).'">'.$page->post_title.'</a></li>';
				?>
				<?php
					$cat = get_category_by_path('projects');
					$category_id = get_cat_ID('projects');
					echo '<li><a href="'.get_category_link( $category_id ).'">'.$cat->name.'</a></li>';
				?>
				<?php
					$cat = get_category_by_path('news');
					$category_id = get_cat_ID('news');
					echo '<li><a href="'.get_category_link( $category_id ).'">'.$cat->name.'</a></li>';
				?>
				<?php
					$page = get_page_by_path('contact');
					echo '<li><a href="'.get_permalink( $page->ID ).'">'.$page->post_title.'</a></li>';
				?>
			</ul>
			<div class="menu-contact">
				<div class="address hidden-xs">
					<ul>
						<li>Moorefield Business Centre</li>
						<li>Moorefield Road, Newbridge</li>
						<li>Co.Kildare, W12 P382</li>
						<li>Ireland</li>
					</ul>
				</div>
				<div class="contact">
					<ul>
						<li>T +353 (0)45 431 011</li>						
						<li>info@jpdarchitects.ie</li>
						<li>
							<a href="https://www.facebook.com/jpdarchitects/"><i class="fa fa-facebook"></i></a>
							<a href="https://www.linkedin.com/company/john-p-delaney-architects"><i class="fa fa-linkedin"></i></a>
							<a href="https://www.instagram.com/jpdarchitects/"><i class="fa fa-instagram"></i></a>										
						</li>
					</ul>
				</div>
			</div>
		</aside>
		<section class="body-container">
			<div class="body-overlay"></div>
			<header>
				<div class="container">
					<div class="header-inner-cont row">
						<?php
							$home_url = home_url().'/';
						?>
						<div class="logo"><a href="<?php echo $home_url; ?>"></a></div>						
						<div class="hamburger-menu">
							<?php
								echo '<img src="'.get_bloginfo("template_directory").'/images/menu-bars.svg">';
							?>
						</div>
						<div class="menu-slide-out hidden-xs">
							<span>Menu</span>
						</div>	
					</div>
				</div>
			</header>
			
			<main class="main-container">