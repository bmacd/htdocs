<?php

	add_theme_support( 'post-thumbnails' ); 

	/**
	 * Hide editor for specific page templates.
	 *
	 */
	function reset_post_editor()
	{
	     global $_wp_post_type_features;

	     $post_type="post";
	     $feature = "editor";
	     if ( !isset($_wp_post_type_features[$post_type]) )
	     {

	     }
	     elseif ( isset($_wp_post_type_features[$post_type][$feature]) )
	     unset($_wp_post_type_features[$post_type][$feature]);
	}

	function reset_page_editor()
	{
	     global $_wp_post_type_features;

	     $post_type="page";
	     $feature = "editor";
	     if ( !isset($_wp_post_type_features[$post_type]) )
	     {

	     }
	     elseif ( isset($_wp_post_type_features[$post_type][$feature]) )
	     unset($_wp_post_type_features[$post_type][$feature]);
	}

	add_action("init","reset_post_editor");
	add_action("init","reset_page_editor");

	function load_cat_parent_template()
	{
	    global $wp_query;

	    if (!$wp_query->is_category)
	        return true; // saves a bit of nesting

	    // get current category object
	    $cat = $wp_query->get_queried_object();

	    // trace back the parent hierarchy and locate a template
	    while ($cat && !is_wp_error($cat)) {
	        $template = TEMPLATEPATH . "/category-{$cat->slug}.php";

	        if (file_exists($template)) {
	            load_template($template);
	            exit;
	        }

	        $cat = $cat->parent ? get_category($cat->parent) : false;
	    }
	}
	add_action('template_redirect', 'load_cat_parent_template');

?>