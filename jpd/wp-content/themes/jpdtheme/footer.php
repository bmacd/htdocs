				<footer>
					<div class="container">
						<div class="row">
							<div class="col-md-2">
								<ul>
									<li>Moorefield Business Centre</li>
									<li>Moorefield Road, Newbridge</li>
									<li>Co.Kildare, W12 P382</li>
									<li>Ireland</li>
								</ul>
							</div>
							<div class="col-md-2">
								<ul>
									<li>T +353 (0)45 431 011</li>
									<li>info@jpdarchitects.ie</li>
									<li>
										<a href="https://www.facebook.com/jpdarchitects/"><i class="fa fa-facebook"></i></a>
										<a href="https://www.linkedin.com/company/john-p-delaney-architects"><i class="fa fa-linkedin"></i></a>
										<a href="https://www.instagram.com/jpdarchitects/"><i class="fa fa-instagram"></i></a>
									</li>
								</ul>
							</div>
							<div class="col-md-8 designed-by">
								<span>Website Design by <a href="http://rorymccormick.ie/">Rory McCormick</a></span>
							</div>
						</div>
					</div>
				</footer>
			</main>		
		</section>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> -->
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script type="text/javascript">
		var markerUrl = "<?php bloginfo('template_directory'); ?>/images/map-marker.png";
	</script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/map.js"></script>	
</html>