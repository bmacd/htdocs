			<?php get_header();?>
			<?php get_sidebar();?>
			<section class="container home-image-slider-container">
				<div class="left-arrow-hover hidden-sm hidden-xs"></div>
				<div class="right-arrow-hover hidden-sm hidden-xs"></div>
				<div class="row">						
					<div class="col-md-8 col-md-offset-2 home-image-slider">
						<div class="image-container">
							<?php
								$slides = get_posts(array(
									'post_type' => 'page',
									'post_status' => 'publish',
									'tag'=>'homepage-slider'
								));								
								$i=0;
								foreach ($slides as $slide) {
									$feat_image = get_field('page_main_image', $slide->ID);
									if($i==0){
										echo 
										'<div class="image-div current" style="background-image: url('.$feat_image.')"></div>'										
										;
									}
									else{
										echo 										
										'<div class="image-div" style="background-image: url('.$feat_image.')"></div>'
										;
									}
									$i++;
								}					
							?>							
						</div>
						<div class="slide-info">
							<?php
								$slides = get_posts(array(
									'post_type' => 'page',
									'post_status' => 'publish',
									'tag'=>'homepage-slider'
								));
								$i=0;
								foreach ($slides as $slide) {
									$slide_cats = get_the_category( $slide->ID );
									if($i==0){
										echo
											'<div class="image-title current">
												'.$slide->post_title.'
												<div class="light">'.$slide_cats[0]->cat_name.'</div>
											</div>'
										;
									}
									else{
										echo
											'<div class="image-title">
												'.$slide->post_title.'
												<div class="light">'.$slide_cats[0]->cat_name.'</div>
											</div>'
										;
									}
									$i++;
								}					
							?>
							<div class="slide-numbers">
								<span class="current-number">1</span>/<span class="total-number"></span>
							</div>
						</div>
					</div>
					<div class="col-md-2 slide-logo hidden-sm hidden-xs">
						<span><img src="<?php bloginfo('template_directory'); ?>/images/jpd-logo.png"></span>
					</div>
				</div>
			</section>

			<section class="about-jpd">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-6 about-text">
							<p>John P Delaney Architects Ltd is an Irish based multidisciplinary practice. With over forty years experience in the planning and design of large and medium scale projects, our knowledge and understanding of architectural place.</p>
						</div>
					</div>
				</div>
			</section>

			<section class="home-projects">
				<div class="container">
					
					<?php
						$pages = get_posts(array(
							'post_type' => 'page',
							'post_status' => 'publish',
							'tag'=>'homepage-featured',
							'posts_per_page' => -1
						));								
						$i=0;
						foreach ($pages as $page) {
							$feat_image = get_field('page_main_image', $page->ID);
							$page_cats = get_the_category( $page->ID );
							$idObj = get_category_by_slug('projects'); 

							foreach((get_the_category($page->ID)) as $childcat) {
								if (cat_is_ancestor_of($idObj, $childcat)) {
									$childcat_name = $childcat->cat_name;	
								}
							}
							
							if($i % 2 == 0){
								echo
								'<div class="row">
									<div class="col-sm-6 left-home-project" style="background-image: url('.$feat_image.')">
										<a href="'.get_page_link($page->ID).'">
											<div class="project-overlay">
												<div class="project-title">
													'.$page->post_title.'
													<span class="category">
													'.$childcat_name.'
													</span>									
												</div>
											</div>
										</a>
									</div>'
								;
							}
							else{
								echo
									'<div class="col-sm-6 right-home-project" style="background-image: url('.$feat_image.')">
										<a href="'.get_page_link($page->ID).'">
											<div class="project-overlay">
												<div class="project-title">
													'.$page->post_title.'
													<span class="category">
													'.$childcat_name.'
													</span>									
												</div>
											</div>
										</a>
									</div>
								</div>'
								;
							}
							$i++;
						}					
					?>	
				</div>
			</section>
			
			<?php get_footer();?>
