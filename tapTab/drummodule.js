    var kitSounds = ['acoustic/kick.mp3', 'acoustic/snare.mp3', 'acoustic/floortom.mp3', 'acoustic/crash.mp3', 'acoustic/ride.mp3', 'acoustic/hihat.mp3', 'acoustic/tom1.mp3', 'acoustic/tom2.mp3'];
    var acousticRadio= $("#acoustic");
    var electricRadio= $("#electric");

    var kickdrum = new audioApiKey("kickdrum", kitSounds[0]);
    var snaredrum = new audioApiKey("snaredrum", kitSounds[1]);
    var floortom = new audioApiKey("floortom", kitSounds[2]);
    var crashcymbal = new audioApiKey("crashcymbal", kitSounds[3]);
    var ridecymbal = new audioApiKey("ridecymbal", kitSounds[4]);
    var hihat = new audioApiKey("hihat", kitSounds[5]);
    var tom1 = new audioApiKey("tom1", kitSounds[6]);
    var tom2 = new audioApiKey("tom2", kitSounds[7]);
     
    var context = new AudioContext();

    // if (document.getElementById("acoustic").checked){
    //   kitSounds=['acoustic/kick.mp3', 'acoustic/snare.mp3', 'acoustic/floortom.mp3', 'acoustic/crash.mp3', 'acoustic/ride.mp3', 'acoustic/hihat.mp3', 'acoustic/tom1.mp3', 'acoustic/tom2.mp3'];
    // }

    // else if (document.getElementById("electric").checked){
    //   kitSounds=['electric/kick.wav', 'electric/snare.wav', 'electric/floortom.wav', 'electric/crash.wav', 'electric/ride.wav', 'electric/hihat.wav', 'electric/tom1.wav', 'electric/tom2.wav'];
    // }

    acousticRadio.click(function(){

        kitSounds=['acoustic/kick.mp3', 'acoustic/snare.mp3', 'acoustic/floortom.mp3', 'acoustic/crash.mp3', 'acoustic/ride.mp3', 'acoustic/hihat.mp3', 'acoustic/tom1.mp3', 'acoustic/tom2.mp3'];

        kickdrum = new audioApiKey("kickdrum", kitSounds[0]);
        snaredrum = new audioApiKey("snaredrum", kitSounds[1]);
        floortom = new audioApiKey("floortom", kitSounds[2]);
        crashcymbal = new audioApiKey("crashcymbal", kitSounds[3]);
        ridecymbal = new audioApiKey("ridecymbal", kitSounds[4]);
        hihat = new audioApiKey("hihat", kitSounds[5]);
        tom1 = new audioApiKey("tom1", kitSounds[6]);
        tom2 = new audioApiKey("tom2", kitSounds[7]);
    });

    electricRadio.click(function(){

      context=null;
      context = new AudioContext();

        kitSounds=['electric/kick.wav', 'electric/snare.wav', 'electric/floortom.wav', 'electric/crash.wav', 'electric/ride.wav', 'electric/hihat.wav', 'electric/tom1.wav', 'electric/tom2.wav'];

        kickdrum = new audioApiKey("kickdrum", kitSounds[0]);
        snaredrum = new audioApiKey("snaredrum", kitSounds[1]);
        floortom = new audioApiKey("floortom", kitSounds[2]);
        crashcymbal = new audioApiKey("crashcymbal", kitSounds[3]);
        ridecymbal = new audioApiKey("ridecymbal", kitSounds[4]);
        hihat = new audioApiKey("hihat", kitSounds[5]);
        tom1 = new audioApiKey("tom1", kitSounds[6]);
        tom2 = new audioApiKey("tom2", kitSounds[7]);
        console.log(kickdrum);
    });

/*	
	else if(document.getElementById('electric').checked==true){
	console.log('electric');
		 var eleckickdrum = new audioApiKey("kickdrum","electric/kick.wav");
		 var elecsnaredrum = new audioApiKey("snaredrum","electric/snare.wav");
		 var elecfloortom = new audioApiKey("floortom","electric/floortom.wav");
		 var eleccrashcymbal = new audioApiKey("crashcymbal","electric/crash.wav");
		 var elecridecymbal = new audioApiKey("ridecymbal","electric/ride.wav");
		 var elechihat = new audioApiKey("hihat","electric/hihat.wav");
		 var electom1 = new audioApiKey("tom1","electric/tom1.wav");
		 var electom2 = new audioApiKey("tom2","electric/tom2.wav");	 
	}
	 var context = new webkitAudioContext();

*/



 function audioApiKey(domNode,fileDirectory) {
	
    this.domNode = domNode;
    this.fileDirectory = fileDirectory;
    var bufferFunctionName = bufferFunctionName;
    var incomingBuffer;
    var savedBuffer;
    var xhr;
		//adds the sound to the buffer 
       bufferFunctionName = function () {
       var source = context.createBufferSource();
       source.buffer = savedBuffer;
       source.connect(context.destination);
       source.start(0); // Play sound immediately
       };
	   //sends the request on the server to get the file
    var xhr = new XMLHttpRequest();
    xhr.open('get',fileDirectory, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function () {
    context.decodeAudioData(xhr.response,
       function(incomingBuffer) {
       savedBuffer = incomingBuffer;
       var note = document.getElementById(domNode);
	   var testcollision= document.getElementById("testcollision");
	   //adds event listener to play the sound when the node is clicked
       note.addEventListener("click", bufferFunctionName , false);
	   
	  
         }
      );
   };
 xhr.send();
 };
 
 function audioApiKeyTab(domNode,fileDirectory) {
  
    this.domNode = domNode;
    this.fileDirectory = fileDirectory;
    var bufferFunctionName = bufferFunctionName;
    var incomingBuffer;
    var savedBuffer;
    var xhr;
    //adds the sound to the buffer 
       bufferFunctionName = function () {
       var source = context.createBufferSource();
       source.buffer = savedBuffer;
       source.connect(context.destination);
       source.start(0); // Play sound immediately
       };
     //sends the request on the server to get the file
    var xhr = new XMLHttpRequest();
    xhr.open('get',fileDirectory, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function () {
    context.decodeAudioData(xhr.response,
       function(incomingBuffer) {
       savedBuffer = incomingBuffer;
       var note = document.getElementById(domNode);
     var tabslider = document.getElementById("tabSlider");
     var testcollision= document.getElementById("testcollision");
     //adds event listener to play the sound when the node is clicked
       note.addEventListener("click", bufferFunctionName , false);
     tabslider.addEventListener("click", bufferFunctionName, false);

     if (collision($('#tabSlider'), $('#tab'))){
      bufferFunctionName();
     }
     
    
         }
      );
   };
 xhr.send();
 };
 


$(function(){
 
  $("#kickdrum").click(function () {
     $(this).animate({ opacity: 0.8}, 50);
	 $(this).animate({ opacity: 1.0}, 50);
 
  });
 
  $("#snaredrum").click(function () {
     $(this).animate({ opacity: 0.8}, 50);
	 $(this).animate({ opacity: 1.0}, 50);
 
  });
  
  $("#floortom").click(function () {
     $(this).animate({ opacity: 0.8}, 50);
	 $(this).animate({ opacity: 1.0}, 50);
 
  });
 
  $("#ridecymbal").click(function () {
     $(this).animate({ opacity: 0.8}, 50);
	 $(this).animate({ opacity: 1.0}, 50);
 
  });
  
  $("#crashcymbal").click(function () {
     $(this).animate({ opacity: 0.8}, 50);
	 $(this).animate({ opacity: 1.0}, 50);
 
  });
 
  $("#hihat").click(function () {
     $(this).animate({ opacity: 0.8}, 50);
	 $(this).animate({ opacity: 1.0}, 50);
 
  });
  
  $("#tom1").click(function () {
     $(this).animate({ opacity: 0.8}, 50);
	 $(this).animate({ opacity: 1.0}, 50);
 
  });
 
  $("#tom2").click(function () {
     $(this).animate({ opacity: 0.8}, 50);
	 $(this).animate({ opacity: 1.0}, 50);
 
  });
 
});