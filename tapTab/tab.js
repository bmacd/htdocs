	$(document).ready(function() {
	var speed = 3000;
	
	var play = $('#play');
	var stop = $('#stop');
	var slow = $('#slow');
	var medium = $('#medium');
	var fast = $('#fast');
    var $grower = $('.tabSlider');
	var $tab = $('.tab');
	var playSpeed=375;
	// var snd = new Audio("acoustic/kick.mp3");
	
	slow.click(function(){
		speed=5000;
		playSpeed=625;
    	function runIt() {
        $grower.animate({
            left: "397px"
        }, speed, 'linear', function() {
            $grower.removeAttr("style");
            runIt();
        });
    }
	
    runIt();
	});
	
	medium.click(function(){
		speed=3000;
		playSpeed=375;
    	function runIt() {
        $grower.animate({
            left: "397px"
        }, speed, 'linear', function() {
            $grower.removeAttr("style");
            runIt();
        });
    }
	
    runIt();
	});
	
	fast.click(function(){
		speed=1000;
		playSpeed=125;
		function runIt() {
        $grower.animate({
            left: "397px"
        }, speed, 'linear', function() {
            $grower.removeAttr("style");
            runIt();
        });
    }
	
    runIt();
	});
	
play.click(function() {
    function runIt() {
        $grower.animate({
            left: "397px"
        }, speed, 'linear', function() {
            $grower.removeAttr("style");
            runIt();
        });
    }
	
    runIt();

    var refreshInterval=setInterval('playSound()', playSpeed);
	});

stop.click(function() {
	$grower.stop();
    $grower.css("left", "0"); 
    clearInterval(refreshInterval);
	});	
	
	
	  


// window.setInterval(function() {
// 	$('#result').text(collision($('#tabSlider'), $('#tab')));}, 250);
 //    $('#result').text(collision($('#tabSlider'), $('#tab')));}, 200);
	
	// if (collision($('#tabSlider'), $('#tab'))){
	// 	snd.play();
	// }


	
	
});

function collision($tabSlider, $tab) {
      var x1 = $tabSlider.offset().left;
      var y1 = $tabSlider.offset().top;
      var h1 = $tabSlider.outerHeight(true);
      var w1 = $tabSlider.outerWidth(true);
      var b1 = y1 + h1;
      var r1 = x1 + w1;
      var x2 = $tab.offset().left;
      var y2 = $tab.offset().top;
      var h2 = $tab.outerHeight(true);
      var w2 = $tab.outerWidth(true);
      var b2 = y2 + h2;
      var r2 = x2 + w2;
        
      if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
      return true;
    }	

function playSound(){

	if(collision($('.tabSlider'), $('.BD .tab'))){
		var tabKick = new Audio(kitSounds[0]);
    	tabKick.play();
	}

	else if(collision($('.tabSlider'), $('.SD .tab'))){
		var tabSnare = new Audio(kitSounds[1]);
    	tabSnare.play();
	}

	else if(collision($('.tabSlider'), $('.FT .tab'))){
		var tabFloor = new Audio(kitSounds[2]);
    	tabFloor.play();
	}

	else if(collision($('.tabSlider'), $('.CC .tab'))){
		var tabCrash = new Audio(kitSounds[3]);
    	tabCrash.play();
	}

	else if(collision($('.tabSlider'), $('.RC .tab'))){
		var tabRide = new Audio(kitSounds[4]);
    	tabRide.play();
	}

	else if(collision($('.tabSlider'), $('.HH .tab'))){
		var tabHat = new Audio(kitSounds[5]);
    	tabHat.play();
	}

	else if(collision($('.tabSlider'), $('.TOne .tab'))){
		var tabTomOne = new Audio(kitSounds[6]);
    	tabTomOne.play();
	}

	else if(collision($('.tabSlider'), $('.TTwo .tab'))){
		var tabTomTwo = new Audio(kitSounds[7]);
    	tabTomTwo.play();
	}

}		

//var kitSounds = ['acoustic/kick.mp3', 'acoustic/snare.mp3', 'acoustic/floortom.mp3', 'acoustic/crash.mp3', 'acoustic/ride.mp3', 'acoustic/hihat.mp3', 'acoustic/tom1.mp3', 'acoustic/tom2.mp3'];