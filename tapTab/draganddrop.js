// function dragStart(ev) {
// 	   ev.dataTransfer.effectAllowed='move';
// 	   ev.dataTransfer.setData("Text", ev.target.getAttribute('id'));   ev.dataTransfer.setDragImage(ev.target,100,100);
// 	   return true;
// 	}
	
// 	function dragIt(theEvent) {
// 	//tell the browser what to drag
// 	helper: 'clone';
// 	theEvent.dataTransfer.setData("Text", theEvent.target.id);
// 	}								
	
// 	function dropIt(theEvent) {
// 	//get a reference to the element being dragged
// 	var theData = theEvent.dataTransfer.getData("Text");
// 	//get the element
// 	var theDraggedElement = document.getElementById(theData);
// 	//add it to the drop element
// 	theEvent.target.appendChild(theDraggedElement);
// 	//instruct the browser to allow the drop
// 	theEvent.preventDefault();
// 	}

$(".tab").draggable({
    helper: 'clone',
    handle: "tab"
});

$(".drop").droppable({
    accept: ".tab",
    drop: function(event, ui){
        $("<i class='tab fa fa-close'></i>")
            .html(ui.draggable.text())
            .appendTo($(this));
    }
});